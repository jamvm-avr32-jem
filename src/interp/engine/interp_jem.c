/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#define _GNU_SOURCE
#include <arpa/inet.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "jam.h"
#include "thread.h"
#include "lock.h"
#include "interp.h"

#include "interp-direct.h"

#include <sys/syscall.h>
#include <unistd.h>
#include <sys/mman.h>

#include <sys/types.h>
#include <linux/unistd.h>

#include <asm/byteorder.h>
#include <asm/cachectl.h>

#include "arch/avr32_jem.h"
#include "interp_jem.h"

union jecr {
	struct {
		uint8_t	dummy;
		uint8_t	opcode;
		uint8_t	op1;
		uint8_t	op2;
	} s;
	uint32_t i;
};

/**
 *  Interpreter execution context
 */
struct intrp_ctx {
	/* JEM state */
	struct jem_state jem;

	/* the interpreter execution context */
	Frame *frame;
	MethodBlock *mb;
	MethodBlock *new_mb;
	ConstantPool *cp;
	Object *this;
	uintptr_t *lvars_jem;
	uintptr_t *ostack;
	uintptr_t *arg1;
	ExecEnv *ee;
};

#define GET_THIS(ctx) do {							\
	if (!((ctx)->mb->access_flags & ACC_STATIC) && (ctx)->mb->max_locals &&	\
		*(ctx)->lvars_jem) {						\
                uintptr_t *__p = (uintptr_t *)*(ctx)->lvars_jem;		\
		(ctx)->this = JAM_OBJECT(__p);					\
	} else									\
		(ctx)->this = NULL;						\
	jam_dprintf("[%s] %smethod with %d vars, assigning %p\n", __func__,	\
		(ctx)->mb->access_flags & ACC_STATIC ? "static " : "",		\
		(ctx)->mb->max_locals, (ctx)->this);				\
} while (0)

typedef int handler_fn(struct intrp_ctx *ctx);

static void jem_opcode_rewrite(uint32_t new_opcode, char *pc, int argc, ...)
{
    int i;
    va_list vargs;

    va_start(vargs, argc);
    pc[0] = new_opcode;
    for (i = 1; i <= argc; i++)
        pc[i] = (char)va_arg(vargs, int);
    va_end(vargs);
    syscall(__NR_cacheflush, CACHE_IFLUSH, (int)pc & ~31, 32);
}

#define min(x, y) ({ typeof(x) _x = (x); typeof(y) _y = (y); _x > _y ? _y : _x; })

//check if it is an valid object ref (see alloc.c)
#define OBJECT_GRAIN  8
#define IS_OBJECT(ptr) !(((uintptr_t)(ptr))&(OBJECT_GRAIN-1))

#define JEM_THROW_EXCEPTION(excep_name, message) \
({						 \
    ctx->frame->last_pc = (CodePntr)ctx->jem.jpc;	 \
    signalException(excep_name, message);	 \
    jem_throwException(ctx);			 \
})

#define JEM_NULL_POINTER_CHECK(ref) \
    if (!ref) JEM_THROW_EXCEPTION("java/lang/NullPointerException", NULL);

/*********************************************
 *     JEM Trap Handlers                    **
 * *******************************************/
static int trap_debug(struct intrp_ctx *ctx)
{
#ifdef JEM_DEBUG
	struct jem_state *jem = &ctx->jem;
	union jecr jecr_u;
	int i;
	static unsigned long count;

	jecr_u.i = jem->jecr;

	jam_printf("[%lu] (%u): Trap 0x%08x: ostack 0x%x, opcode 0x%x @ 0x%08x, "
		   "operand 1 0x%x, operand 2 0x%x. Operands:\n",
		   count++, pthread_self(), jem->trap_pc, ctx->ostack, jecr_u.s.opcode,
		   jem->jpc, jecr_u.s.op1, jecr_u.s.op2);

	for (i = 0; i < jem->josp; i++) {
	    uint32_t v;
	    if (ostack_read_u32(ctx->frame->ostack, ctx->ostack, i, &v) >= 0)
		jam_printf("\tToS-%d: 0x%x\n", i, v);
	}
#endif
	return 1;
}


static int jem_throwException(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    ExecEnv *ee = ctx->ee;
    Object *excep = ee->exception;
    ee->exception = NULL;

    jem->jpc = (unsigned long)findCatchBlock(excep->class);
    jam_dprintf("Found exception handler at 0x%08x\n", jem->jpc);

    /* If we didn't find a handler, restore exception and
       return to previous invocation */

    if (!jem->jpc) {
        ee->exception = excep;
        /* Original code had a "return NULL" here, which means, end of
         * executeJava */
        return JEM_TRAP_HANDLER_FINISH;
    }

    /* If we're handling a stack overflow, reduce the stack
       back past the red zone to enable handling of further
       overflows */

    if (ee->overflow) {
        ee->overflow = FALSE;
        ee->stack_end -= STACK_RED_ZONE_SIZE;
    }

    /* Setup intepreter to run the found catch block */

    ctx->frame = ee->last_frame;
    ctx->mb = ctx->frame->mb;
    ctx->ostack = ctx->frame->ostack;
    ctx->lvars_jem = ctx->frame->lvars_jem - 1;

    GET_THIS(ctx);

    ctx->cp = &(CLASS_CB(ctx->mb->class)->constant_pool);

    /* FIXME: don't know if this is correct, depends how we implement
     * exceptions */
    ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, (uint32_t)excep);

    /* Dispatch to the first bytecode */
    return 0;
}

static int opc_return(struct intrp_ctx *ctx)
{
    ExecEnv *ee = ctx->ee;

    /* Set interpreter state to previous frame */
    if (ctx->frame->prev->mb == NULL) {
       /* The previous frame is a dummy frame - this indicates
        * top of this Java invocation. */
	return JEM_TRAP_HANDLER_FINISH;
    }

    ctx->frame = ctx->frame->prev;

    jam_dprintf("[%s] return to frame %p mb %p\n", __func__, ctx->frame, ctx->frame->mb);

    if (ctx->mb->access_flags & ACC_SYNCHRONIZED) {
        Object *sync_ob = ctx->mb->access_flags & ACC_STATIC ? (Object*)ctx->mb->class : ctx->this;
        jam_dprintf("[%s] %smethod unlock obj %p\n", __func__,
            ctx->mb->access_flags & ACC_STATIC ? "static " : "", sync_ob);
        objectUnlock(sync_ob);
    }

    ctx->ostack = ctx->lvars_jem - ctx->mb->max_locals + 1 - ctx->mb->args_count;
    ctx->mb = ctx->frame->mb;
    ctx->lvars_jem = ctx->frame->lvars_jem - 1;

    GET_THIS(ctx);

    ctx->jem.jpc = (uint32_t)ctx->frame->last_pc;
    ctx->cp = &(CLASS_CB(ctx->mb->class)->constant_pool);
    jam_dprintf("[OPC_RETURN] ostack: %p, lvars_jem: %p this: %p class %p\n",
		ctx->ostack, ctx->lvars_jem, ctx->this,
		ctx->this ? ctx->this->class : NULL);

    /* Pop frame */ 
    ee->last_frame = ctx->frame;

    return 0;
}

static int invokeMethod(struct intrp_ctx *ctx)
{
    ExecEnv *ee = ctx->ee;
    /* Seems like the original jamvm reused top of caller's stack for callee's
     * local variables for passing of method arguments. Our stack and
     * local variables grow in opposite directions now, so, this is impossible.
     * ctx->ostack points at the beginning of the free space above ostack.
     */
    Frame *new_frame = (Frame *)(ctx->ostack + ctx->new_mb->max_locals);
    Object *sync_ob = NULL;
    int i;
    uintptr_t *aobj;

    jam_dprintf("[invokeMethod] with args count %d, frame %p, return to 0x%08x\n",
		ctx->new_mb->args_count, new_frame, ctx->jem.jpc);

    ctx->frame->last_pc = (CodePntr)ctx->jem.jpc;
    ctx->ostack = (uintptr_t *)(new_frame + 1);

    if (ostack_overflows(ctx->ostack, ctx->new_mb->max_stack, (uintptr_t *)ee->stack_end)) {
        if (ee->overflow++) {
            /* Overflow when we're already throwing stack overflow.
               Stack extension should be enough to throw exception,
               so something's seriously gone wrong - abort the VM! */
            jam_printf("Fatal stack overflow!  Aborting VM.\n");
            exitVM(1);
        }
        ee->stack_end += STACK_RED_ZONE_SIZE;
        return JEM_THROW_EXCEPTION("java/lang/StackOverflowError", NULL);
    }

    //TODO 
    new_frame->mb = ctx->new_mb;
    //TODO: it seemed lvars is in java stack, but jos[8] should be part of java stack
    //      lvars_jem requires that the highest pointer should refer to the first local variables LV0
    //      Here still not touch the java stack, allocate lvars_jem from native heap

    new_frame->lvars_jem = (uintptr_t *)new_frame;

    for (i = 0; i < ctx->new_mb->max_locals; i++) {
        if (i < ctx->new_mb->args_count) {
            *(new_frame->lvars_jem - 1 - i) = *(ctx->arg1 + i);
            jam_dprintf("arg[%d] = 0x%x@%p\n", i, *(ctx->arg1 + i), ctx->arg1 + i);
        }
#ifdef JEM_DEBUG
        else
            *(new_frame->lvars_jem - 1 - i) = 0xdeadbeef;
#endif
    }

    jam_dprintf("[invokeMethod] %s::%s() lvars_jem %p\n",
                CLASS_CB(ctx->new_mb->class)->name, ctx->new_mb->name, new_frame->lvars_jem);

    new_frame->ostack = ctx->ostack;
    new_frame->prev = ctx->frame;

    ee->last_frame = new_frame;

    if (ctx->new_mb->access_flags & ACC_SYNCHRONIZED) {
        aobj = (uintptr_t *)*ctx->arg1;
        sync_ob = ctx->new_mb->access_flags & ACC_STATIC ?
		(Object*)ctx->new_mb->class : JAM_OBJECT(aobj);
        objectLock(sync_ob);
    }

    if (ctx->new_mb->access_flags & ACC_NATIVE) {
        //FIXME: the object references in JEM are direct pointer to instance variables.
        //       While JamVM requires object references as pointers to Object type.
        jam_dprintf("[invokeMethod] invoke native method using %p invoker\n",
                    ctx->new_mb->native_invoker);
        ctx->ostack = (*(uintptr_t *(*)(Class*, MethodBlock*, uintptr_t*))
                      ctx->new_mb->native_invoker)(ctx->new_mb->class, ctx->new_mb, ctx->arg1);

        if (sync_ob)
             objectUnlock(sync_ob);

        ee->last_frame = ctx->frame;

        if (exceptionOccured0(ee)) {
            jam_dprintf("[invokeMethod] exception occured, goto throwException\n");
            return jem_throwException(ctx);
        }

        jam_dprintf("[invokeMethod] finish native method invoke, return to loop\n");
    } else {
        ctx->frame = new_frame;
        ctx->mb = ctx->new_mb;
        ctx->lvars_jem = new_frame->lvars_jem - 1;

        GET_THIS(ctx);

        ctx->jem.jpc = (unsigned long)ctx->mb->code & ~3;
        ctx->cp = &(CLASS_CB(ctx->mb->class)->constant_pool);

        jam_dprintf("[invokeMethod] invoke virtual method: mb %x "
                    "lvars %x jpc %x cp %x\n", ctx->mb,
                    ctx->lvars_jem, ctx->jem.jpc, ctx->cp);
     }

     return 0;
}

static int opc_invokesuper_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;

    jam_dprintf("[OPC_INVOKESUPER_QUICK] method table indx %d\n", operand->i);
    ctx->new_mb = CLASS_CB(CLASS_CB(ctx->mb->class)->super)->method_table[operand->i];
    jam_dprintf("[OPC_INVOKESUPER_QUICK] method args count %d\n", ctx->new_mb->args_count);

    // set up jem operand stack
    ctx->arg1 = ostack_address(ctx->frame->ostack, ctx->mb->max_stack,
			  ctx->ostack - ctx->frame->ostack - ctx->new_mb->args_count);
    JEM_NULL_POINTER_CHECK(*ctx->arg1);
    jam_dprintf("[OP_INVOKESUPER_QUICK] arg1 %x\n", *ctx->arg1);
    return invokeMethod(ctx);
}

static int opc_invokenonvirtual_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;

    ctx->new_mb = (MethodBlock*)operand->pntr;
    jam_dprintf("[%s] new methodblock %s (%x) with %d args\n",
	       __func__, ctx->new_mb->name, ctx->new_mb, ctx->new_mb->args_count);
    ctx->arg1 = ostack_address(ctx->frame->ostack, ctx->mb->max_stack,
			  ctx->ostack - ctx->frame->ostack - ctx->new_mb->args_count);

    jam_dprintf("[%s] ctx->arg1 %x\n", __func__, *ctx->arg1);
    JEM_NULL_POINTER_CHECK(*ctx->arg1);
    return invokeMethod(ctx);
}

static int opc_invokevirtual_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Object *obj;
    ctx->arg1 = ostack_address(ctx->frame->ostack, ctx->mb->max_stack,
			ctx->ostack - ctx->frame->ostack - INV_QUICK_ARGS(jem));
    JEM_NULL_POINTER_CHECK(*ctx->arg1);
    Class *new_class;

    //in case of "AALOAD" loads JAM obj from reference array
    obj = (Object *)*ctx->arg1;
    if (!JAM_ON_STACK || !IS_JAM_OBJECT(obj)) {
        obj = JAM_OBJECT((uintptr_t *)obj);
        //we should ensure ostack contains no JAM object
        *ctx->arg1 = (uintptr_t)INST_DATA(obj);
    }

    jam_dprintf("[OPC_INVOKEVIRTUAL_QUICK] invoke on object %p from %p class %p\n",
                obj, ctx->arg1, obj->class);
    new_class = obj->class;
    ctx->new_mb = CLASS_CB(new_class)->method_table[INV_QUICK_IDX(jem)];
    jam_dprintf("[OPC_INVOKEVIRTUAL_QUICK] invoke method 0x%08x\n",
		ctx->new_mb);
    return invokeMethod(ctx);
}

static int opc_invokevirtual(struct intrp_ctx *ctx)
{
    int idx;
    ExecEnv *ee = ctx->ee;
    struct jem_state *jem = &ctx->jem;

    idx = jem->jecr & 0xffff;
    ctx->frame->last_pc = (CodePntr)jem->jpc;
    ctx->new_mb = resolveMethod(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
	return jem_throwException(ctx);

    jem->operand.uu.u1 = ctx->new_mb->args_count;
    jem->operand.uu.u2 = ctx->new_mb->method_table_index;

    jam_dprintf("[OPC_INVOKEVIRTUAL] invoke 0x%08x with args count %d\n",
               ctx->new_mb, ctx->new_mb->args_count);

    return opc_invokevirtual_quick(ctx);
}

static int opc_invokespecial(struct intrp_ctx *ctx)
{
    int idx;
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    ExecEnv *ee = ctx->ee;

    /* INVOKESPECIAL's operands represent an index into the Constant Pool */
    idx = jem->jecr & 0xffff;

    jam_dprintf("[OPC_INVOKESPECIAL] constant pool index %d\n", idx);

    ctx->new_mb = resolveMethod(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    jam_dprintf("Resolved %s\n", ctx->new_mb->name);

    /* Check if invoking a super method... */
    if ((CLASS_CB(ctx->mb->class)->access_flags & ACC_SUPER) &&
	((ctx->new_mb->access_flags & ACC_PRIVATE) == 0) && (ctx->new_mb->name[0] != '<')) {

        operand->i = ctx->new_mb->method_table_index;
	return opc_invokesuper_quick(ctx);
#if 0 /* FIXME: verify this whole "pc" chemistry is indeed unneeded */
	OPCODE_REWRITE(OPC_INVOKESUPER_QUICK, cache, operand);
#endif
    } else {
        operand->pntr = ctx->new_mb;
        return opc_invokenonvirtual_quick(ctx);
    }

    return 0;
}

static int opc_invokestatic_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;

    ctx->new_mb = RESOLVED_METHOD(jem);
    ctx->arg1 = ostack_address(ctx->frame->ostack, ctx->mb->max_stack,
			  ctx->ostack - ctx->frame->ostack - ctx->new_mb->args_count);
    return invokeMethod(ctx);
}

static int opc_invokestatic(struct intrp_ctx *ctx)
{
    int idx;
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    ExecEnv *ee = ctx->ee;

    idx = jem->jecr & 0xffff;

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    ctx->new_mb = resolveMethod(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    operand->pntr = ctx->new_mb;
    return opc_invokestatic_quick(ctx);
}

static int opc_new_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    Class *class = (Class*)CP_INFO(ctx->cp, operand->uui.u1);
    Object *obj;

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    if ((obj = allocObject(class)) == NULL)
        return jem_throwException(ctx);
    jam_dprintf("[OPC_NEW_QUICK] push obj ref %x to stack\n", obj);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
			   (uint32_t)INST_DATA(obj));
}

static int opc_new(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    int idx = jem->jecr & 0xffff;
    int opcode = (jem->jecr >> 16) & 0xff;
    Class *class;
    ExecEnv *ee = ctx->ee;

    operand->uui.u1 = idx;
    operand->uui.u2 = opcode;

    jam_dprintf("[OPC_NEW] opcode: 0x%x, index: 0x%x\n", opcode, idx);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    class = resolveClass(ctx->mb->class, idx, opcode == OPC_NEW);
    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    if (opcode == OPC_NEW) {
        ClassBlock *cb = CLASS_CB(class);
        if (cb->access_flags & (ACC_INTERFACE | ACC_ABSTRACT)) {
            signalException("java/lang/InstantiationError", cb->name);
            return jem_throwException(ctx);
        }
        /* rewrite opcode to OPC_NEW_QUICK (0xDD) */
        jem_opcode_rewrite(0xDD, (char*)jem->jpc - 3, 0);
    }

    return opc_new_quick(ctx);
}

static int opc_anewarray_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    ConstantPool *cp = ctx->cp;
    Class *class = RESOLVED_CLASS(jem);
    char *name = CLASS_CB(class)->name;
    int count;
    Class *array_class;
    char *ac_name;
    Object *obj;
    ExecEnv *ee = ctx->ee;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, count);
    ctx->frame->last_pc = (CodePntr)jem->jpc;

    jam_dprintf("[OPC_ANEWARRAY_QUICK] array %s size %d\n", name, count);
    if (count < 0) {
        signalException("java/lang/NegativeArraySizeException", NULL);
        return jem_throwException(ctx);
    }

    ac_name = sysMalloc(strlen(name) + 4);

    if (name[0] == '[')
        sprintf(ac_name, "[%s", name);
    else
        sprintf(ac_name, "[L%s;", name);

    array_class = findArrayClassFromClass(ac_name, ctx->mb->class);
    free(ac_name);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    if ((obj = allocArray(array_class, count, sizeof(Object*))) == NULL)
        return jem_throwException(ctx);

    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
			   (uint32_t)ARRAY_DATA(obj));
}

static int opc_anewarray(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    int idx = jem->jecr & 0xffff;
    int opcode = (jem->jecr >> 16) & 0xff;
    Class *class;
    ExecEnv *ee = ctx->ee;

    operand->uui.u1 = idx;
    operand->uui.u2 = opcode;

    jam_dprintf("[OPC_ANEWARRAY] index: 0x%x\n", idx);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    //resolve the array class
    class = resolveClass(ctx->mb->class, idx, FALSE);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    return opc_anewarray_quick(ctx);
}

static int opc_multianewarray_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    ConstantPool *cp = ctx->cp;
    Class *class = RESOLVED_CLASS(jem);
    int i, dim = MULTI_ARRAY_DIM(jem);
    int count;
    Object *obj;

    ctx->frame->last_pc = (CodePntr)jem->jpc;

    jam_dprintf("[OPC_MULTIANEWARRAY_QUICK]: alloc %d dimensional array for class %s\n", dim, CLASS_CB(class)->name);
    for (i = 0; i < dim; i++) {
        ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, count);
        jam_dprintf("[OPC_MULTIANEWARRAY_QUICK]: elements %d for dim %d\n", count, i);
        if (count < 0) {
            signalException("java/lang/NegativeArraySizeException", NULL);
            return jem_throwException(ctx);
        }
    }

    /* counts are still stored in ostack even after we did pop them out */
    if ((obj = allocMultiArray(class, dim, (intptr_t *)ctx->ostack)) == NULL)
        return jem_throwException(ctx);

    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
                           (uint32_t)ARRAY_DATA(obj));
}

static int opc_multianewarray(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    int idx = operand->uui.u1 = jem->jecr & 0xffff;

    Class *class;
    ExecEnv *ee = ctx->ee;

    /* dimensions */
    operand->uui.u2 = *(unsigned char*)jem->jpc;
    jem->jpc++; /* skip dims */

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    /* resolve the array class */
    class = resolveClass(ctx->mb->class, idx, FALSE);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    return opc_multianewarray_quick(ctx);
}

static int opc_newarray(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    int type = (jem->jecr >> 8) & 0xff;
    int count;
    Object *obj;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, count);
    ctx->frame->last_pc = (CodePntr)jem->jpc;

    jam_dprintf("[OPC_NEWARRAY] alloc array type %d, count %d\n", type, count);
    if ((obj = allocTypeArray(type, count)) == NULL)
        return jem_throwException(ctx);

    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
                           (uint32_t)ARRAY_DATA(obj));
}

static int opc_putfield_quick(struct intrp_ctx *ctx)
{
    uint32_t arg;
    uint32_t obj;
    struct jem_state *jem = &ctx->jem;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, arg);
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, obj);
    jam_dprintf("[OPC_PUTFIELD_QUICK] put field %x to obj(%d) %x\n", arg,
                SINGLE_INDEX(jem), obj);

    JEM_NULL_POINTER_CHECK(obj);

    if (JAM_ON_STACK && IS_JAM_OBJECT(obj))
        obj = (uint32_t)INST_DATA((Object *)obj);

    ((uint32_t*)obj)[SINGLE_INDEX(jem)] = arg;

    return 0;
}

union ull_2ui {
	long long ll;
	unsigned int i[2];
};

static int opc_putfield2_quick(struct intrp_ctx *ctx)
{
    uint32_t obj;
    struct jem_state *jem = &ctx->jem;
    union ull_2ui v;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v.i);
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, obj);

    JEM_NULL_POINTER_CHECK((uintptr_t*)obj);

    if (JAM_ON_STACK && IS_JAM_OBJECT(obj))
        obj = (uint32_t)INST_DATA((Object *)obj);

    *(uint64_t *)&(((uint32_t*)obj)[SINGLE_INDEX(jem)]) = v.ll;
    jam_dprintf("[%s] put 64 bit field 0x%llx to obj %08x\n", __func__, v.ll, obj);

    return 0;
}

static int opc_putfield_quick_jem0(struct intrp_ctx *ctx)
{
    uint32_t arg;
    uintptr_t *aobj;
    struct jem_state *jem = &ctx->jem;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, arg);
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, aobj);
    jam_dprintf("[OPC_PUTFIELD_QUICK_JEM0]  field %x\n", arg);

    JEM_NULL_POINTER_CHECK(aobj);

    if (JAM_ON_STACK && arg && !IS_JAM_OBJECT(arg))
        arg = (uint32_t)JAM_OBJECT((uintptr_t *)arg);

    if (JAM_ON_STACK && IS_JAM_OBJECT(aobj))
        aobj = INST_DATA((Object *)aobj);

    aobj[SINGLE_INDEX(jem)] = arg;
    jam_dprintf("[OPC_PUTFIELD_QUICK_JEM0] put field %x to obj(%d) %x\n", arg,
                SINGLE_INDEX(jem), aobj);

    return 0;
}

static int opc_putfield_quick_jem1(struct intrp_ctx *ctx)
{
    uintptr_t *aobj, *arg;
    struct jem_state *jem = &ctx->jem;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, arg);
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, aobj);
    jam_dprintf("[OPC_PUTFIELD_QUICK_JEM1]  field %p\n", arg);

    if (JAM_ON_STACK && arg && !IS_JAM_ARRAY(arg))
        arg = (uintptr_t *)JAM_ARRAY(arg);

    if (JAM_ON_STACK && IS_JAM_OBJECT(aobj))
        aobj = INST_DATA((Object *)aobj);

    JEM_NULL_POINTER_CHECK(aobj);
    aobj[SINGLE_INDEX(jem)] = (uintptr_t)arg;
    jam_dprintf("[OPC_PUTFIELD_QUICK_JEM1] put field %p to obj(%d) %p\n", arg,
                SINGLE_INDEX(jem), aobj);

    return 0;
}

static int opc_putfield(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    int idx = jem->jecr & 0xffff;
    FieldBlock *fb;
    ExecEnv *ee = ctx->ee;

    jam_dprintf("[OPC_PUTFIELD] constant pool index %d\n", idx);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    fb = resolveField(ctx->mb->class, idx);
    jam_dprintf("[OPC_PUTFIELD] resolve field 0x%08x, type %c, name %s\n", fb, *fb->type, fb->name);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    jem->operand.i = fb->offset;
    switch (*fb->type) {
    case 'J':
    case 'D':
        /* rewrite opc_putfield to opc_putfield2_quick */
        jem_opcode_rewrite(0xD1, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
        return opc_putfield2_quick(ctx);
    case 'L':
        jem_opcode_rewrite(0xCF, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
        return opc_putfield_quick_jem0(ctx);
    case '[':
        jem_opcode_rewrite(0xCF, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
        return opc_putfield_quick_jem1(ctx);
    default:
        jem_opcode_rewrite(0xCF, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
        return opc_putfield_quick(ctx);
    }
}

static int opc_getfield2_quick(struct intrp_ctx *ctx)
{
    uint32_t *obj;
    struct jem_state *jem = &ctx->jem;
    union ull_2ui v;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, obj);

    JEM_NULL_POINTER_CHECK(obj);

    if (JAM_ON_STACK && IS_JAM_OBJECT(obj))
        obj = (uint32_t*)INST_DATA((Object*)obj);

    v.ll = *(uint64_t*)(&(obj[SINGLE_INDEX(jem)]));
    jam_dprintf("[%s] get 64 bit field 0x%llx from obj %08x\n", __func__, v.ll, obj);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v.i);
}

static int opc_getfield_quick(struct intrp_ctx *ctx)
{
    uintptr_t *aobj;
    struct jem_state *jem = &ctx->jem;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, aobj);
    JEM_NULL_POINTER_CHECK(aobj);

    if (JAM_ON_STACK && IS_JAM_OBJECT(aobj))
        aobj = (uint32_t*)INST_DATA((Object*)aobj);

    jam_dprintf("[OPC_GETFIELD_QUICK] get  field %08x from obj %08x\n",
                aobj[SINGLE_INDEX(jem)], aobj);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
                           aobj[SINGLE_INDEX(jem)]);
}

static int opc_getfield_quick_jem0(struct intrp_ctx *ctx)
{
    uint32_t *aobj;
    struct jem_state *jem = &ctx->jem;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, aobj);
    JEM_NULL_POINTER_CHECK(aobj);

    if (JAM_ON_STACK && IS_JAM_OBJECT(aobj))
        aobj = (uint32_t*)INST_DATA((Object*)aobj);

    uint32_t value = aobj[SINGLE_INDEX(jem)];
    if (value) {
        Object *ref = (Object*)value; 
        if (JAM_ON_STACK && IS_JAM_OBJECT(ref))
            //jamvm objref
            value = (uint32_t)INST_DATA(ref);
        jam_dprintf("[OPC_GETFIELD_QUICK_JEM0] obj %p ref %x, class %p : %p\n",
		    aobj, value, JAM_OBJECT((void *)value)->class, ref->class);
    }
    jam_dprintf("[OPC_GETFIELD_QUICK_JEM0] put obj ref %x\n", value);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);
}

static int opc_getfield_quick_jem1(struct intrp_ctx *ctx)
{
    uint32_t *aobj;
    struct jem_state *jem = &ctx->jem;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, aobj);
    JEM_NULL_POINTER_CHECK(aobj);

    //"aaload" would put jamvm obj ref onto the ostack
    if (JAM_ON_STACK && IS_JAM_OBJECT(aobj))
        aobj = (uint32_t*)INST_DATA((Object*)aobj);

    uint32_t value = aobj[SINGLE_INDEX(jem)];
    jam_dprintf("[OPC_GETFIELD_QUICK_JEM1] array ref %x\n", value);
    if (value) {
        Object *ref = (Object*)value; 
        if (JAM_ON_STACK && IS_JAM_ARRAY(ref))
            value = (uint32_t)ARRAY_DATA(ref);
    }
    jam_dprintf("[OPC_GETFIELD_QUICK_JEM1] put array ref %x\n", value);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);
}

static int opc_getfield(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    int idx = jem->jecr & 0xffff;
    FieldBlock *fb;
    ExecEnv *ee = ctx->ee;

    jam_dprintf("[OPC_GETFIELD] constant pool index %d\n", idx);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    fb = resolveField(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    jem->operand.i = fb->offset;

    jam_dprintf("[OPC_GETFIELD] resolve field %s @ %p, type %c, offset %i, class %p\n",
		fb->name, fb, *fb->type, jem->operand.i, fb->class);

    switch (*fb->type) {
    case 'J':
    case 'D':
        jem_opcode_rewrite(0xD0, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
	return opc_getfield2_quick(ctx);
    /*
     * Though we assume all objrefs/arrayrefs here should be JEM compliant, but
     * in case of some JNI code will set field natively, we still handle it
     * separately here.
     */
    case 'L':
        jem_opcode_rewrite(0xCE, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
        return opc_getfield_quick_jem0(ctx);
    case '[':
        jem_opcode_rewrite(0xCE, (char*)jem->jpc - 3, 2, fb->offset >> 8, fb->offset);
        return opc_getfield_quick_jem1(ctx);
    default:
        //jem_opcode_rewrite(0xCE, (char*)jem->jpc - 3, 2, (fb->offset>>8)&0xff, (fb->offset)&0xff);
	return opc_getfield_quick(ctx);
    }
}

static int opc_ldc2_w(struct intrp_ctx *ctx)
{
    int idx;
    union ull_2ui v;
    struct jem_state *jem = &ctx->jem;

    idx = (jem->jecr & 0xffff);
    jam_dprintf("[OPC_LDC2_W] constant pool index %d\n", idx);
    jem->operand.i = idx;

    //read long long from constant pool
    v.ll = (uint64_t)CP_LONG(ctx->cp, DOUBLE_INDEX(jem));
    jam_dprintf("[OPC_LDC2_W] push constant 0x%llx to stack\n", v.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v.i);
}

static int opc_ldc_w_quick_jem(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    //CONSTANT_Class and CONSTANT_String are stored as object reference
    Object *cp_info = (Object*)CP_INFO(ctx->cp, DOUBLE_INDEX(jem));
    jam_dprintf("[OPC_LDC_W_QUICK_JEM] push String ref  %x to stack\n", cp_info);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
			   (uint32_t)INST_DATA(cp_info));
}

static int opc_ldc_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    uint32_t constant = RESOLVED_CONSTANT(jem);
    jam_dprintf("[OPC_LDC_QUICK] push constant %x to stack\n", constant);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, (uintptr_t)constant);
}

static int opc_ldc_common(struct intrp_ctx *ctx, int idx)
{
    struct jem_state *jem = &ctx->jem;
    ExecEnv *ee = ctx->ee;
    ctx->frame->last_pc = (CodePntr)jem->jpc;

    jem->operand.u = resolveSingleConstant(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    switch (CP_TYPE(ctx->cp, idx)) {
    case CONSTANT_ResolvedClass:
    case CONSTANT_ResolvedString:
        jem->operand.i = idx;
        return opc_ldc_w_quick_jem(ctx);
    default:
        jem_opcode_rewrite(0xCB, (char*)jem->jpc - 2, 0);
        return opc_ldc_quick(ctx);
    }
}

static int opc_ldc(struct intrp_ctx *ctx)
{
    int idx;
    struct jem_state *jem = &ctx->jem;
		    	
    idx = (jem->jecr & 0xff00) >> 8;

    jam_dprintf("[OPC_LDC] constant pool index %d\n", idx);
    return opc_ldc_common(ctx, idx);
}

static int opc_ldc_w(struct intrp_ctx *ctx)
{
    int idx;
    struct jem_state *jem = &ctx->jem;

    idx = (jem->jecr & 0xffff);

    jam_dprintf("[OPC_LDC_W] constant pool index %d\n", idx);
    return opc_ldc_common(ctx, idx);
}

static int opc_monitorenter(struct intrp_ctx *ctx)
{
    Object *obj;
    uintptr_t *ptr;
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, ptr);

    obj = JAM_OBJECT(ptr);
    if (!IS_OBJECT(obj))
        /* is an array ref */
        obj = JAM_ARRAY(ptr);
    jam_dprintf("[OPC_MONITORENTER] lock %x\n", obj);
    JEM_NULL_POINTER_CHECK(obj);
    objectLock(obj);
    return 0;
}

static int opc_monitorexit(struct intrp_ctx *ctx)
{
    Object *obj;
    uintptr_t *ptr;
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, ptr);

    obj = JAM_OBJECT(ptr);
    if (!IS_OBJECT(obj))
        /* is an array ref */
        obj = JAM_ARRAY(ptr);
    jam_dprintf("[OPC_MONITOREXIT] unlock %x\n", obj);
    JEM_NULL_POINTER_CHECK(obj);
    objectUnlock(obj);
    return 0;
}

static int opc_getstatic_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
			   RESOLVED_FIELD(jem)->static_value);
}

static int opc_getstatic2_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    union ull_2ui v;

    v.ll = RESOLVED_FIELD(jem)->static_value;

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v.i);
}

static int opc_getstatic_quick_jem0(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    uintptr_t value = RESOLVED_FIELD(jem)->static_value;

    jam_dprintf("[OPC_GETSTATIC_QUICK_JEM0] obj ref %x\n", value);
    if (value) {
	Object *ref = (Object *)value; 
	if (!JAM_ON_STACK || IS_JAM_OBJECT(ref))
	    //jamvm objref
	    value = (uintptr_t)INST_DATA(ref);
    }
    jam_dprintf("[OPC_GETSTATIC_QUICK_JEM0] put obj ref %x\n", value);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);
}

static int opc_getstatic_quick_jem1(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    uintptr_t value = RESOLVED_FIELD(jem)->static_value;
    jam_dprintf("[OPC_GETSTATIC_QUICK_JEM1] array ref %x\n", value);
    if (value) {
	Object *ref = (Object*)value; 
	if (!JAM_ON_STACK || IS_JAM_ARRAY(ref))
	    //jamvm arrayref
	    value = (uintptr_t)ARRAY_DATA(ref);
    }
    jam_dprintf("[OPC_GETSTATIC_QUICK_JEM1] put array ref %x\n", value);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);
}

static int opc_getstatic(struct intrp_ctx *ctx)
{
    int idx;
    FieldBlock *fb;
    ExecEnv *ee = ctx->ee;
    struct jem_state *jem = &ctx->jem;

    idx = (int)(jem->jecr & 0xffff);
    jam_dprintf("[OPC_GETSTATIC] constant pool index %d class %s\n",idx,CLASS_CB(ctx->mb->class)->name);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    fb = resolveField(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    jam_dprintf("[OPC_GETSTATIC] get static for field %08x, type %c\n", fb, *fb->type);
    jem->operand.pntr = fb;
    switch (*fb->type) {
    case 'J':
    case 'D':
	return opc_getstatic2_quick(ctx);
    /*
     * see opc_getfield
     */
    case 'L':
	return opc_getstatic_quick_jem0(ctx);
    case '[':
	return opc_getstatic_quick_jem1(ctx);
    default:
	return opc_getstatic_quick(ctx);
    }
}

static int opc_putstatic2_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    return ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
                          &RESOLVED_FIELD(jem)->static_value);
}

static int opc_putstatic_quick(struct intrp_ctx *ctx)
{ 
    struct jem_state *jem = &ctx->jem;
    return ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
			  RESOLVED_FIELD(jem)->static_value);
}

static int opc_putstatic_quick_jem0(struct intrp_ctx *ctx)
{
    uintptr_t value;
    struct jem_state *jem = &ctx->jem;
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);

    jam_dprintf("[OPC_PUTSTATIC_QUICK_JEM0] obj %x\n", value);
    if (value && (!JAM_ON_STACK || !IS_JAM_OBJECT(value)))
	value = (uintptr_t)JAM_OBJECT((uintptr_t *)value);
    jam_dprintf("[OPC_PUTSTATIC_QUICK_JEM0] put obj %x\n", value);
    RESOLVED_FIELD(jem)->static_value = value;
    return 0;
}

static int opc_putstatic_quick_jem1(struct intrp_ctx *ctx)
{
    uintptr_t *value;
    struct jem_state *jem = &ctx->jem;
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);

    jam_dprintf("[OPC_PUTSTATIC_QUICK_JEM1] array %p\n", value);
    if (value && (!JAM_ON_STACK || !IS_JAM_ARRAY(value)))
	value = (uintptr_t*)JAM_ARRAY(value);
    jam_dprintf("[OPC_PUTSTATIC_QUICK_JEM1] put array %p\n", value);
    RESOLVED_FIELD(jem)->static_value = (uintptr_t)value;
    return 0;
}

static int opc_putstatic(struct intrp_ctx *ctx)
{
    int idx;
    FieldBlock *fb;
    ExecEnv *ee = ctx->ee;
    struct jem_state *jem = &ctx->jem;

    idx = (int)(jem->jecr & 0xffff);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    fb = resolveField(ctx->mb->class, idx);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    jam_dprintf("[OPC_PUTSTATIC] put static for field %08x, type %c\n", fb, *fb->type);
    jem->operand.pntr = fb;
    switch (*fb->type) {
    case 'J':
    case 'D':
        //jem_opcode_rewrite(0xD5, (char*)jem->jpc - 3);
	return opc_putstatic2_quick(ctx);
    case 'L':
	return opc_putstatic_quick_jem0(ctx);
    case '[':
	return opc_putstatic_quick_jem1(ctx);
    default:
	return opc_putstatic_quick(ctx);
    }
}

static int opc_checkcast_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    ConstantPool *cp = ctx->cp;
    Class *class = RESOLVED_CLASS(jem);
    Object *obj;
    uintptr_t *ptr;

    ostack_read_u32(ctx->frame->ostack, ctx->ostack, 0, &ptr);
    jam_dprintf("[OPC_CHECKCAST_QUICK] check cast for JEM type %x (class %s)\n", ptr, CLASS_CB(class)->name);
    if (ptr != NULL) {
#warning Does not look right!
        if (!JAM_ON_STACK || !IS_JAM_OBJECT(ptr) || !IS_JAM_ARRAY(ptr)) {
            obj = JAM_OBJECT(ptr);
            if (!IS_OBJECT(obj)) {
                jam_dprintf("[OPC_CHECKCAST_QUICK] check cast for array\n");
                //should be array ref
                obj = JAM_ARRAY(ptr);
            }
            jam_dprintf("[OPC_CHECKCAST_QUICK] check cast for object %x\n", obj);
        } else {
            obj = (Object*)ptr;
        }
        jam_dprintf("[OPC_CHECKCAST_QUICK] check class %p (%s) for obj %p (%s)\n",
                    class, CLASS_CB(class)->name, obj->class, CLASS_CB(obj->class)->name);
        if (!isInstanceOf(class, obj->class))
            return JEM_THROW_EXCEPTION("java/lang/ClassCastException", CLASS_CB(obj->class)->name);
    }

    return 0;
}

static int opc_checkcast(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    int idx = jem->jecr & 0xffff;
    int opcode = (jem->jecr >> 16) & 0xff;
    Class *class;
    ExecEnv *ee = ctx->ee;

    operand->uui.u1 = idx;
    operand->uui.u2 = opcode;

    jam_dprintf("[OPC_CHECKCAST] index: 0x%x\n", idx);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    //resolve the cast class
    class = resolveClass(ctx->mb->class, idx, FALSE);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    return opc_checkcast_quick(ctx);
}

static int opc_invokeinterface_quick(struct intrp_ctx *ctx)
{
    int mtbl_idx;
    ClassBlock *cb;
    Object *objref;
    int cache = ctx->jem.operand.uu.u2;

    ctx->new_mb = (MethodBlock *)CP_INFO(ctx->cp, ctx->jem.operand.uu.u1);
    ctx->arg1 = ostack_address(ctx->frame->ostack, ctx->mb->max_stack,
			  ctx->ostack - ctx->frame->ostack - ctx->new_mb->args_count);

    jam_dprintf("[OPC_INVOKEINTERFACE_QUICK] newmb %x (name %s) (args count %d)  objref(jem) %x\n",
                ctx->new_mb, ctx->new_mb->name, ctx->new_mb->args_count, ctx->arg1);

    objref = (Object *)*ctx->arg1;
#warning Does not look right!
    if (!JAM_ON_STACK || !IS_JAM_OBJECT(objref) || !IS_JAM_ARRAY(objref)) {
        objref = JAM_OBJECT((uintptr_t *)objref);
        if (!IS_OBJECT(objref))
            objref = JAM_ARRAY((uintptr_t *)*ctx->arg1);
    } else {
        //turn type in the stack to JEM type ref
        *ctx->arg1 = (uintptr_t)INST_DATA(objref);
    }
    JEM_NULL_POINTER_CHECK(objref);

    cb = CLASS_CB(objref->class);

    if ((cache >= cb->imethod_table_size) ||
               (ctx->new_mb->class != cb->imethod_table[cache].interface)) {
        for (cache = 0; (cache < cb->imethod_table_size) &&
                            (ctx->new_mb->class != cb->imethod_table[cache].interface); cache++);

        if (cache == cb->imethod_table_size)
            return JEM_THROW_EXCEPTION("java/lang/IncompatibleClassChangeError",
                              "unimplemented interface");
    }

    mtbl_idx = cb->imethod_table[cache].offsets[ctx->new_mb->method_table_index];
    jam_dprintf("[OPC_INVOKEINTERFACE_QUICK] find method at %d\n", mtbl_idx);
    ctx->new_mb = cb->method_table[mtbl_idx];
    jam_dprintf("[OPC_INVOKEINTERFACE_QUICK] invoking method at %d\n", mtbl_idx);
    return invokeMethod(ctx);
}

static int opc_invokeinterface(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    ExecEnv *ee = ctx->ee;

    int idx = operand->uu.u1 = jem->jecr & 0xffff;
    /*NOTES: invokeinterface opcode is supposed to take 4 operands.
     *       For JEM, two index bytes has been processed before trap,
     *       so offset jpc 4 operands for next opcode
     *FIXME: ? not know why JEM does not handle this */
    ctx->frame->last_pc = (CodePntr)(jem->jpc += 2);
    ctx-> new_mb = resolveInterfaceMethod(ctx->mb->class, idx);

    jam_dprintf("[OPC_INVOKEINTERFACE] constant pool index %d\n", idx);
    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    if (CLASS_CB(ctx->new_mb->class)->access_flags & ACC_INTERFACE) {
	operand->uu.u2 = 0;
        return opc_invokeinterface_quick(ctx);
    } else {
        operand->uu.u1 = ctx->new_mb->args_count;
        operand->uu.u2 = ctx->new_mb->method_table_index;
        return opc_invokevirtual_quick(ctx);
    }
}

static int opc_instanceof_quick(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    ConstantPool *cp = ctx->cp;
    Class *class = RESOLVED_CLASS(jem);
    Object *obj;
    uintptr_t *ptr;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, ptr);
    if (ptr != NULL) {
        obj = JAM_OBJECT(ptr);
        if (!IS_OBJECT(obj)) {
	        jam_dprintf("[OPC_INSTANCEOF_QUICK] instanceof for array\n");
	        //should be array ref
	        obj = JAM_ARRAY(ptr);
        }

        jam_dprintf("[OPC_INSTANCEOF_QUICK] check instanceof class %s for obj(%s)\n",
                    CLASS_CB(class)->name, CLASS_CB(obj->class)->name);
    }
    ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack,
                    ptr ? (uint32_t)isInstanceOf(class, obj->class) : 0);
    return 0;
}

static int opc_instanceof(struct intrp_ctx *ctx)
{
    struct jem_state *jem = &ctx->jem;
    Operand *operand = &jem->operand;
    int idx = jem->jecr & 0xffff;
    int opcode = (jem->jecr >> 16) & 0xff;
    Class *class;
    ExecEnv *ee = ctx->ee;

    operand->uui.u1 = idx;
    operand->uui.u2 = opcode;

    jam_dprintf("[OPC_INSTANCEOF] index: 0x%x\n", idx);

    ctx->frame->last_pc = (CodePntr)jem->jpc;
    //resolve the cast class
    class = resolveClass(ctx->mb->class, idx, FALSE);

    if (exceptionOccured0(ee))
        return jem_throwException(ctx);

    return opc_instanceof_quick(ctx);
}

static int opc_aastore(struct intrp_ctx *ctx)
{
    Object *obj;
    uintptr_t *aobj;
    int idx ;
    Object *array;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, aobj);
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, idx);
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, array);

    jam_dprintf("[OPC_AASTORE] obj %p, index %d, array %p\n", aobj, idx, array);

    /* aobj does point to an object, but we do not know if that's an array or
     * not, and we need to know that for the arrayStoreCheck() check below */
#warning Does not look right!
    if (aobj && (!JAM_ON_STACK || !IS_JAM_OBJECT(aobj) || !IS_JAM_ARRAY(aobj))) {
        if (!IS_OBJECT(JAM_OBJECT(aobj))) {
            //in case of storing an array to another array
            obj = JAM_ARRAY(aobj);
        } else {
            obj = JAM_OBJECT(aobj);
        }
    } else
        obj = (Object *)aobj;
    array = JAM_ARRAY((uintptr_t *)array);

    jam_dprintf("[OPC_AASTORE] store obj ref %p of %p to array %p[%d/%d] of %p\n",
		obj, obj ? obj->class : obj, array, idx, array->instance[0], array->class);
    JEM_NULL_POINTER_CHECK(array);

    if (idx >= ARRAY_LEN(array)) {
        char buff[MAX_INT_DIGITS];
        snprintf(buff, MAX_INT_DIGITS, "%d", idx);
        JEM_THROW_EXCEPTION("java/lang/ArrayIndexOutOfBoundsException", buff);
    }

    if (obj != NULL && !arrayStoreCheck(array->class, obj->class))
        JEM_THROW_EXCEPTION("java/lang/ArrayStoreException", NULL);

    //FIXME: If we set JEM ref here, it is very likely that some native jamvm code would directly offset
    //       the array to get the non jamvm refs to use, which then causes segfault.
#if JAM_ON_STACK
    ((Object**)ARRAY_DATA(array))[idx] = obj;
#else
    ((uintptr_t**)ARRAY_DATA(array))[idx] = aobj;
#endif
    return 0;
}

static int do_javaExceptions(struct intrp_ctx *ctx)
{
    union jecr jecr_u;
    struct jem_state *jem = &ctx->jem;

    /* deliberate segfault */
    /* *(int *)0 = 0; */

    jecr_u.i = jem->jecr;
    /* This should not happen, because we don't use the "Java Handle" (H)
     * bit in the Status Register and we set JBCR to 0x80000000 - still
     * it does happen, but seems to be harmless so far. Just ignore. */
    if (jecr_u.s.opcode == OPC_AASTORE)
	    return opc_aastore(ctx);

    return 1;
}

static int opc_afireturn(struct intrp_ctx *ctx)
{
    uint32_t val;
    int ret;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, val);

    jam_dprintf("[OPC_AFIRETURN] return 0x%08x @ %p, this %p\n",
		val, ctx->lvars_jem, ctx->this);

    ret = opc_return(ctx);
    if (!ret)
	ret = ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, val);
    else
        /* Function returning a value must have local variables */
        *ctx->lvars_jem = val;

    return ret;
}

static int opc_ldreturn(struct intrp_ctx *ctx)
{
    int ret;
    union ull_2ui v;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v.i);
    jam_dprintf("[OPC_LDRETURN] return 0x%llx\n", v.ll);

    ret = opc_return(ctx);

    if (!ret)
        ret = ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack,
			      ctx->ostack, v.i);
    else
        /* Function returning a value must have local variables */
        *(uint64_t*)ctx->lvars_jem = v.ll;

    return ret;
}

static int opc_fcmpg(struct intrp_ctx *ctx)
{
    float v1, v2;
    int res;

    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1);

    if (v1 == v2)
        res = 0;
    else if (v1 < v2)
       res = -1;
    else if (v1 > v2)
       res = 1;
    else
       res = 1; /* isNaN */

    jam_dprintf("[OPC_FCMPG] result %d\n", res);

    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res);
}

static int opc_fcmpl(struct intrp_ctx *ctx)
{
    float v1, v2;
    int res;

    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1);

    if (v1 == v2)
	res = 0;
    else if (v1 < v2)
	res = -1;
    else if (v1 > v2)
	res = 1;
    else
	res = -1;

    jam_dprintf("[OPC_FCMPL] result %d\n", res);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res);
}

static int opc_lcmp(struct intrp_ctx *ctx)
{
    union ull_2ui v1, v2;
    int res;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2.i);
    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1.i);

    if (v1.ll == v2.ll)
	res = 0;
    else if (v1.ll - v2.ll > 0LL)
	res = 1;
    else
	res = -1;

    jam_dprintf("[%s] (0x%llx,0x%llx) result %d\n", __func__, v1.ll, v2.ll, res);
    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res);
}

static int opc_i2f(struct intrp_ctx *ctx)
{
    int i;
    float f;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, i);
    f = (float)i;

    jam_dprintf("[OPC_I2F] convert to %f\n", f);

    return ostack_push_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, f);
}

static int opc_i2d(struct intrp_ctx *ctx)
{
    return JEM_TRAP_HANDLER_FINISH;
}

static int opc_ldiv(struct intrp_ctx *ctx)
{
    union ull_2ui v1, v2, res;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2.i);
    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1.i);

    res.ll = v1.ll / v2.ll;

    jam_dprintf("[%s] (0x%llx,0x%llx) result 0x%llx\n", __func__, v1.ll, v2.ll, res.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res.i);
}

static int opc_lrem(struct intrp_ctx *ctx)
{
    union ull_2ui v1, v2, res;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2.i);
    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1.i);

    res.ll = v1.ll % v2.ll;

    jam_dprintf("[%s] (0x%llx,0x%llx) result 0x%llx\n", __func__, v1.ll, v2.ll, res.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res.i);
}

static int opc_lmul(struct intrp_ctx *ctx)
{
    union ull_2ui v1, v2, res;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2.i);
    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1.i);

    res.ll = v1.ll * v2.ll;

    jam_dprintf("[%s] (0x%llx,0x%llx) result 0x%llx\n", __func__, v1.ll, v2.ll, res.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res.i);
}

static int opc_lushr(struct intrp_ctx *ctx)
{
    union ull_2ui res, v1;
    unsigned long v2;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1.i);

    res.ll = v1.ll >> (v2 & 0x3f);

    jam_dprintf("[%s] (0x%llx,%u) result 0x%llx\n", __func__, v1.ll, v2, res.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res.i);
}

static int opc_lshr(struct intrp_ctx *ctx)
{
    union ull_2ui v1, res;
    unsigned long v2;

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1.i);

    res.ll = (long long)v1.ll / (1LL << (v2 & 0x3f));

    jam_dprintf("[%s] (0x%llx,%u) result 0x%llx\n", __func__, v1.ll, v2, res.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res.i);
}

static int opc_lneg(struct intrp_ctx *ctx)
{
    union ull_2ui v, res;

    ostack_pop_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v.i);

    res.ll = -v.ll;

    jam_dprintf("[%s] (0x%llx) result 0x%llx\n", __func__, v.ll, res.ll);

    return ostack_push_u64(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res.i);
}

static int opc_fmul(struct intrp_ctx *ctx)
{
    float v1, v2;
    float ret;

    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1);

    ret = v1 * v2;

    jam_dprintf("[OPC_FMUL] result %f\n", ret);

    return ostack_push_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, ret);
}

static int opc_fadd(struct intrp_ctx *ctx)
{
    float v1, v2;
    float ret;

    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1);

    ret = v1 + v2;

    jam_dprintf("[OPC_FADD] result %f\n", ret);

    return ostack_push_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, ret);
}

static int opc_fsub(struct intrp_ctx *ctx)
{
    float v1, v2;
    float ret;

    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v2);
    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, v1);

    ret = v1 - v2;

    jam_dprintf("[OPC_FSUB] result %f\n", ret);

    return ostack_push_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, ret);
}

static int opc_f2i(struct intrp_ctx *ctx)
{
    int res;
    float value;

    ostack_pop_f32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, value);

    jam_dprintf("[OPC_F2I] float value %f\n", value);
    if (value >= (float)INT_MAX)
	res = INT_MAX;
    else if (value <= (float)INT_MIN)
	res = INT_MIN;
    else if (value != value)
	res = 0;
    else
	res = (int)value;

    jam_dprintf("[OPC_F2I] int value %d\n", res);

    return ostack_push_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, res);
}

static int opc_iinc(struct intrp_ctx *ctx)
{
    union jecr  jecr_u;
    jecr_u.i = ctx->jem.jecr;
    int index = (int)jecr_u.s.op1;
    int con = (int)jecr_u.s.op2;
    uintptr_t *ptr = ctx->lvars_jem;

    jam_dprintf("[OPC_IINC] add %d to local var %x at %d (%d)\n",
                con, ptr[-index], index, ctx->mb->max_locals);
    ptr[-index] = ptr[-index] + con;

    jam_dprintf("[OPC_IINC] local var is %x now\n", ptr[-index]);
    return 0;
}

static int opc_tableswitch(struct intrp_ctx *ctx)
{
    /* On entry JPC points to "default" */
    int32_t dflt, high, low, *dflt_p;
    int32_t idx;
    uint32_t target;
    uint8_t *tswitch;

    tswitch = (uint8_t *)(ctx->jem.jpc - 3);
    dflt_p = (int32_t *)(ctx->jem.jpc & ~3);

    dflt = __be32_to_cpu(*dflt_p);
    low = __be32_to_cpu(*(dflt_p + 1));
    high = __be32_to_cpu(*(dflt_p + 2));

    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, idx);

    if (idx < low || idx > high)
        target = dflt;
    else
        target = __be32_to_cpu(*(dflt_p + 3 + idx - low));

#ifdef JEM_DEBUG
    for (i = 0; i <= high - low; i++) {
        int32_t *addr = dflt_p + 3 + i;

        if (!(i & 15))
            jam_dprintf("%p:", addr);
        jam_dprintf(" 0x%x", *addr);
        if ((i & 15) == 15 || i == high - low)
            jam_dprintf("\n", addr);
    }
#endif

    jam_dprintf("[%s] trap @%p, jpc 0x%x, default 0x%x @ %p, low %d, high %d, idx %d, target 0x%x\n",
                __func__, tswitch, ctx->jem.jpc, dflt, dflt_p, low, high, idx, target);

    ctx->jem.jpc = (uint32_t)(tswitch + target);

    return 0;
}

static int opc_athrow(struct intrp_ctx *ctx)
{
    Object *obj;
    ostack_pop_u32(ctx->frame->ostack, ctx->mb->max_stack, ctx->ostack, obj);
    ctx->frame->last_pc = (CodePntr)ctx->jem.jpc;
    ExecEnv *ee = ctx->ee;

    JEM_NULL_POINTER_CHECK(obj);

    if (!JAM_ON_STACK || !IS_JAM_OBJECT(obj))
        obj = JAM_OBJECT((uintptr_t *)obj);

    jam_dprintf("[OPC_ATHROW] throw exception %s\n", CLASS_CB(obj->class)->name);
    ee->exception = obj;
    return jem_throwException(ctx);
}

static int do_stackOverflow(struct intrp_ctx *ctx)
{
    jam_dprintf("[TRAP] STACK OVERFLOW XXXXXXXX\n");
    return 0;
}

static int do_stackUnderflow(struct intrp_ctx *ctx)
{
    jam_dprintf("[TRAP] STACK UNDERFLOW XXXXXXXX\n");
    return 1;
}

static int do_stackOverflow1(struct intrp_ctx *ctx)
{
    jam_dprintf("[TRAP] STACK OVERFLOW1XXXXXXXX\n");
    return 1;
}

static int do_stackUnderflow1(struct intrp_ctx *ctx)
{
   jam_dprintf("[TRAP] STACK UNDERFLOW1XXXXXXXX\n");
   return 1;
}

static handler_fn *trap_handler_f[256] = {
    [OPC_INVOKEINTERFACE]		= opc_invokeinterface,
    [OPC_INVOKESPECIAL]			= opc_invokespecial,
    [OPC_INVOKESUPER_QUICK]		= opc_invokesuper_quick,
    [OPC_INVOKENONVIRTUAL_QUICK]	= opc_invokenonvirtual_quick,
    [OPC_INVOKEVIRTUAL]			= opc_invokevirtual,
    [OPC_INVOKEVIRTUAL_QUICK]		= opc_invokevirtual_quick,
    [OPC_INVOKESTATIC]			= opc_invokestatic,
//    [OPC_INVOKESTATIC_QUICK]		= opc_invokestatic_quick,
    [OPC_NEW]				= opc_new,
    [0xDD]			        = opc_new,		/* JEM: NEW_QUICK */
    [OPC_RETURN]			= opc_return,
    [OPC_ARETURN]			= opc_afireturn,
    [OPC_IRETURN]			= opc_afireturn,
    [OPC_FRETURN]			= opc_afireturn,
    [OPC_PUTFIELD]			= opc_putfield,
//    [OPC_PUTFIELD_QUICK]		= opc_putfield_quick,
//    [OPC_PUTFIELD2_QUICK]		= opc_putfield2_quick,
//    [OPC_LDC_W_QUICK]			= opc_ldc_w_quick,
//    [OPC_LDC_QUICK]			= opc_ldc_quick,
    [OPC_LDC_W]				= opc_ldc_w,
    [OPC_LDC]				= opc_ldc,
    [OPC_LDC2_W]			= opc_ldc2_w,
    [OPC_MONITORENTER]			= opc_monitorenter,
    [OPC_MONITOREXIT]			= opc_monitorexit,
    [OPC_GETSTATIC]			= opc_getstatic,
//    [OPC_GETSTATIC_QUICK]		= opc_getstatic_quick,
//    [OPC_GETSTATIC2_QUICK]		= opc_getstatic2_quick,
    [OPC_PUTSTATIC]			= opc_putstatic,
    [OPC_PUTSTATIC_QUICK]		= opc_putstatic_quick,
    [OPC_PUTSTATIC2_QUICK]		= opc_putstatic2_quick,
    [OPC_ANEWARRAY]			= opc_anewarray,
    [OPC_ANEWARRAY_QUICK]		= opc_anewarray_quick,
    [OPC_MULTIANEWARRAY]		= opc_multianewarray,
    [OPC_NEWARRAY]			= opc_newarray,
    [OPC_CHECKCAST]			= opc_checkcast,
    [OPC_CHECKCAST_QUICK]		= opc_checkcast_quick,
    [OPC_GETFIELD]			= opc_getfield,
//    [OPC_GETFIELD_QUICK]		= opc_getfield_quick,
//    [OPC_GETFIELD2_QUICK]		= opc_getfield2_quick,
    [OPC_LSHR]				= opc_lshr,
    [OPC_LUSHR]				= opc_lushr,
    [OPC_LCMP]				= opc_lcmp,
    [OPC_LNEG]				= opc_lneg,
    [OPC_LDIV]				= opc_ldiv,
    [OPC_LREM]				= opc_lrem,
    [OPC_LMUL]				= opc_lmul,
    [OPC_FCMPL]				= opc_fcmpl,
    [OPC_FCMPG]				= opc_fcmpg,
    [OPC_I2F]				= opc_i2f,
    [OPC_I2D]				= opc_i2d,
    [OPC_FMUL]				= opc_fmul,
    [OPC_FADD]				= opc_fadd,
    [OPC_FSUB]				= opc_fsub,
    [OPC_F2I]				= opc_f2i,
    [OPC_IINC]				= opc_iinc,
    [OPC_INSTANCEOF]			= opc_instanceof,
    [OPC_ATHROW]			= opc_athrow,
    [OPC_LRETURN]			= opc_ldreturn,
    [OPC_TABLESWITCH]			= opc_tableswitch,
};

static handler_fn *exception_handler_f[] = {
	[0] = do_javaExceptions,
	[1] = do_stackOverflow,
	[2] = do_stackUnderflow,
	[3] = do_stackOverflow1,
	[4] = do_stackUnderflow1,
};

struct jem_profile {
	unsigned long time;
	unsigned int n;
};

#ifdef JEM_PROFILE
static struct jem_profile prof[256];
#endif

/******************************/
uintptr_t *executeJava(void)
{
/**
 * load trap entries to JEM trap table set when initializing JamVM
 */
    int i;
    uint32_t jep;
    int done = 0;
#ifdef JEM_PROFILE
    static struct timeval tv1, tv2;
#endif
    ExecEnv *ee = getExecEnv();

    /* Initialize interpreter's static environment */
    struct intrp_ctx ctx;
    struct jem_state *jem = &ctx.jem;

    ctx.ee = ee;
    ctx.frame = ee->last_frame;
    ctx.mb = ctx.frame->mb;
    /*
     * We use ostack to save overflowing Java Operand Stack elements. On a stack
     * overflow we free a half of the stack (4 elements) to avoid an immediate
     * next overflow. When entering a trap, the Operand Stack is saved in a
     * temporary array in the reverse order:
     * ATTENTION: Looks like ToS is really in r0, not in r7, as JEM manual says
     * jos[0]     = r7 (?)
     * ...
     * jos[7 - N] = rN (ToS - N)
     * ...
     * jos[7]     = r0 (ToS)
     * UPDATE (27.06.2008)
     * I think, actually, the manual is right in some way... If you access
     * registers individually _when_ JOSP == N + 1 != 0. If they are read out
     * with an ldm, you get this:
     * jos[0]     = r7 (?)
     * ...
     * jos[7 - N] = rN (ToS)
     * ...
     * jos[7]     = r0 (ToS - N)
     * on an overflow we push r3..r0, i.e., jos[4..7] to the frame stack, and
     * set saved JOSP to 4.
     */
    unsigned long jos[8];

    ctx.ostack = ctx.frame->ostack;
    ctx.cp = &(CLASS_CB(ctx.mb->class)->constant_pool);
    /* End enterpreter's static environment */
    
    //fetch the original pc before prepare direct mb
    //TODO: 
    /* I think, mb->code is indeed a copy of the original code[] array from the
     * class object. See class.c::defineClass():390 */

    if (ctx.frame->lvars_jem) {
	ctx.lvars_jem = ctx.frame->lvars_jem - 1;

	GET_THIS(&ctx);
    } else {
        jam_printf("BUG: lvars_jem == NULL!!!");
        exitVM(1);
    }

    jam_dprintf("[executeJava] ostack %08x locals %d lvars_jem %08x\n",
		ctx.ostack, ctx.mb->max_locals, ctx.lvars_jem);
    jam_dprintf("[executeJava] execute method %s on class %s\n",
		ctx.mb->name, CLASS_CB(ctx.mb->class)->name);

#if 0
    /* Don't think we need it, at least, not in the direct.c form. There the code
     * is converted into an internal representation, which is useless for JEM */
    PREPARE_MB(ctx.mb);//see direct.c (prepare)
#endif

    jem->jpc = (unsigned long)ctx.mb->code & ~3;
    jam_dprintf("[executeJava] begin on opcode %04x (%04x)\n",
		*(char*)jem->jpc, *((char*)jem->jpc + 1));

    do {
#ifdef JEM_PROFILE
	/* This is not thread-safe. Have to use TLS. */
	static
#endif
		int opcode;

#ifdef JEM_DEBUG
	jam_dprintf("About to enter JEM at 0x%08x, this = %p, class = %p. Code:",
		    jem->jpc, ctx.this, ctx.this ? ctx.this->class : NULL);
	for (i = 0; i < 32 && jem->jpc + i < (unsigned long)(ctx.mb->code + ctx.mb->code_size); i++) {
		if (!(i & 0xf))
			jam_dprintf("\n0x%04x:", i);
		jam_dprintf(" 0x%02x", ((char*)jem->jpc)[i]);
	}

	jam_dprintf("\nUp to 8 first Local Variables out of %d:\n", ctx.mb->max_locals);
	for (i = 0; i < min(8, ctx.mb->max_locals); i++)
		jam_dprintf("LVAR_%d: 0x%08x\n", i, *(ctx.lvars_jem - i), ctx.lvars_jem - i);
#endif

	/* On entry we need last Java Operand Stack depth, set it not deeper
	 * than half hardware stack */
	jem->josp = min(5, ostack_depth(ctx.frame->ostack, ctx.ostack));
	jam_dprintf("Pop %d words to JEM stack from %p:%p\n", jem->josp,
		    ctx.frame->ostack, ctx.ostack);
	/* As long as we are in asm, top jem.josp stack elements belong JEM */
	for (i = 0; i < jem->josp; i++)
		ostack_pop_u32(ctx.frame->ostack, ctx.mb->max_stack, ctx.ostack,
			       jos[7 - jem->josp + 1 + i]);

#ifdef JEM_PROFILE
	if (tv1.tv_sec) {
		gettimeofday(&tv2, NULL);
		prof[opcode].time += (tv2.tv_sec - tv1.tv_sec) * 1000000 +
			tv2.tv_usec - tv1.tv_usec;
		prof[opcode].n++;
	}
#endif

	__asm__ __volatile__(
		".globl debug_jem_enter\n"
		"debug_jem_enter:\n"
		"	pushm	r0-r7,r10-r11\n"
		"	mov	r8, %[cp]\n"
		"	mov	r9, %[lv0]\n"
		"	mov	lr, %[jpc]\n"
		"	mov	r12, %[josp]\n"		/* Cannot use %[josp] ... */
		"	mov	r11, %[ostack]\n"
		"	pushm	r11\n"			/* Need ostack below */
		"	ldm	r11, r0-r7\n"		/* Restore Java Operands */
		"	cp.w	r12, 0\n"		/* Test josp == 0 */
		"	breq	4f\n"
		"5:	incjosp	1\n"			/* ... for loop counter, */
		"	sub	r12, 1\n"		/* because it can be */
		"	brne	5b\n"			/* one of r0-r7 */
		"4:	popjc\n"			/* Restore Local Variables */
		"	sub	r10, pc, . - trap\n"	/* These 3 instructions */
		"	retj\n"				/* have to stay */
		"trap:	pushjc\n"			/* together */
		"	mfsr	r11, 0x58\n"		/* JOSP */
		"	lsl	r11, 28\n"		/* Clear r11[31:3] using */
		"	lsr	r11, 28\n"		/* one register */
		"	mov	r8, r11\n"
		"	breq	2f\n"
		"3:	incjosp	-1\n"
		"	sub	r11, 1\n"
		"	brne	3b\n"
		"2:	popm	r11\n"			/* ostack */
		"	stm	r11, r0-r7\n"		/* Save Java Operands */
		"	popm	r0-r7,r10-r11\n"
		"	mov	%[tpc], r12\n"
		"	mov	%[jpc], lr\n"		/* Resume address */
		"	mfsr	%[jecr], 0x54\n"	/* JECR */
		"	mov	%[josp], r8\n"		/* JOSP */
		"	.globl debug_jem_exit\n"
		"debug_jem_exit:\n"
		: [jecr] "=r" (jem->jecr), [tpc] "=r" (jem->trap_pc),
		  [josp] "+r" (jem->josp), [jpc] "+r" (jem->jpc)
		: [cp] "r" (ctx.cp->info), [lv0] "r" (ctx.lvars_jem),
		  [ostack] "r" (jos)
		: "r8", "r9", "lr", "r12", "sp", "memory", "cc"
		);
	/*
	 * trap_pc now holds trap pc, describing which trap has been entered,
	 * jecr holds JECR with the trapping bytecode at bits 23..16, possibly
	 *	arguments at bits 7..0 and 15..8,
	 * josp holds JOSP (JOSP & 0xf),
	 * jos holds stored registers r7-r0 (note order!) - Java Operand Stack
	 * jpc points at the trapping opcode, in case of exceptions and stack
	 *	over- and underflows, and at the next opcode, in case of a trap
	 */

#ifdef JEM_PROFILE
	gettimeofday(&tv1, NULL);
#endif

	for (i = 0; i < jem->josp; i++)
		ostack_push_u32(ctx.frame->ostack, ctx.mb->max_stack, ctx.ostack,
				jos[7 - i]);

	opcode = (jem->jecr >> 16) & 0xff;

	jep = jem->trap_pc & 0xfff;

	trap_debug(&ctx);
	if (jep < 0x280)
	    done = exception_handler_f[jep >> 7](&ctx);
	else if (trap_handler_f[opcode])
	    done = trap_handler_f[opcode](&ctx);
	else {
	    /* Unimplemented */
	    jam_printf("Unimplemented Java Opcode 0x%02x\n", opcode);
	    exitVM(1);
	}

#ifdef JEM_DEBUG
	usleep(100);
#endif
    } while (done != JEM_TRAP_HANDLER_FINISH);

    return ctx.ostack;

//    DISPATCH_FIRST;

    jam_dprintf("Unknown Java Opcodes\n");
    exitVM(1);
}

#ifndef executeJava
void initialiseInterpreter(InitArgs *args) {
    initialiseDirect(args);
}
#endif

void dump_profile(void)
{
#ifdef JEM_PROFILE
	int i;

	for (i = 0; i < 256; i++)
		if (prof[i].n)
			jam_printf("0x%x:\t%u times,\t%lu usec\n", i, prof[i].n, prof[i].time);
#endif
}
