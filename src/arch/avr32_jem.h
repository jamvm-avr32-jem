/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef AVR32_JEM_H_
#define AVR32_JEM_H_
#ifdef JEM

#define JEM_TRAP_ENTRIES 25 //our own trap table entries,the first entry is a jump to the proper offset
//extern static const void* jem_trap_handlers_0_ENTRY[JEM_TRAP_ENTRIES];

//extern char *trap_h;

#define __NR_java_settrap 282

/**
  * Called when exit jam vm
  * TODO:
  */
#define DESTORY_JEM_TRAPS

enum {
	JEM_TRAP_HANDLER_FINISH = 1,
	JEM_TRAP_HANDLER_SYSEX,
	JEM_TRAP_HANDLER_UNIMPL,
};

struct jem_state {
	uint32_t jecr;
	uint32_t trap_pc;
	uint32_t josp;
	uint32_t jpc;
        union ins_operand operand;   //point to operands used by next opcode
};

/* #define JEM_DEBUG */

#ifdef JEM_DEBUG
#define jam_dprintf(arg...) jam_printf(arg)
#else
#define jam_dprintf(arg...) do {} while (0)
#endif

#define ostack_push_f32 ostack_push_u32
#define ostack_pop_f32 ostack_pop_u32

#define OSTACK_ASCENDING

#ifdef OSTACK_ASCENDING

#define ostack_overflows(base, size, top) ((base) + (size) >= (top))

#ifndef OSTACK_DEBUG

#define ostack_push_u32(base, size, top, data) ({	\
	*(typeof(data) *)(top)++ = (data);		\
	0;						\
})

#define ostack_pop_u32(base, size, top, data) ({	\
	(data) = *(typeof(data) *)--(top);		\
	0;						\
})

/*
 * JEM hardware stores 64-bit values on stack as two 32-bit words: word2 above,
 * word1 below, where word2 holds high order, and word1 low order bytes. With
 * stack growing from low to high addresses on a big-endian platform this makes:
 * w1b3 w1b2 w1b1 w1b0 w2b3 w2b2 w2b1 w2b0
 * whereas a native 64-bit word is stored as
 *   b7   b6   b5   b4   b3   b2   b1   b0
 * which means inverted word order. We have to fix it by swapping them.
 */
/*
 * Prototypes:
 * int ostack_push_u64(base, size, unsigned int *top, unsigned int *data);
 */
#define ostack_push_u64(base, size, top, data) ({	\
	*(top) = *((data) + 1);				\
	*((top) + 1) = *(data);				\
	top = (typeof(top))((char *)(top) + 8);		\
	0;						\
})

#define ostack_pop_u64(base, size, top, data) ({	\
	*((data) + 1) = *((top) - 2);			\
	*(data) = *((top) - 1 );			\
	top = (typeof(top))((char *)(top) - 8);		\
	0;						\
})

#define ostack_read_u32(base, top, offset, data) ({	\
	*(data) = *(typeof(data))((top) - (offset) - 1);\
	0;						\
})
/*
#define ostack_push_f32(base, size, top, data) ({	\
	*(float *)(top) = (data);			\
	(top)++;					\
	0;						\
})

#define ostack_pop_f32(base, size, top, data)  ({	\
	(data) = *(float *)--(top);			\
	0;						\
})
*/
#define ostack_address(base, size, offset) ((base) + (offset))

#define ostack_depth(base, top) ((top) - (base))

#else

#define ostack_push_u32(base, size, top, data) ({	\
	if ((size) <= (top) - (base))			\
		-ENOMEM;				\
	else {						\
		*(top)++ = (data);			\
		0;					\
	}						\
})

#define ostack_pop_u32(base, size, top, data) ({	\
	if ((top) <= (base))				\
		-ENOMEM;				\
	else {						\
		(data) = *--(top);			\
		0;					\
	}						\
})

#error FIXME: fix *_u64 ops to swap words as above

#define ostack_push_u64(base, size, top, data) ({		\
	if ((size) <= (top) - (base))				\
		-ENOMEM;					\
	else {							\
		*(uint64_t *)(top) = *(uint64_t *)(data);	\
		(top) = (typeof(top))((uint64_t *)top + 1);	\
		0;						\
	}							\
})

#define ostack_pop_u64(base, size, top, data) ({		\
	if ((top) <= (base))					\
		-ENOMEM;					\
	else {							\
		(top) = (typeof(top))((uint64_t *)(top) - 1);	\
		*(uint64_t *)(data) = *(uint64_t *)(top);	\
		0;						\
	}							\
})

#define ostack_read_u32(base, top, offset, data) ({	\
	uint32_t _o = (offset), _t = (top);		\
	if (_o > _t - (base))				\
		-ENOMEM;				\
	else {						\
		*(data) = *(_t - _o - 1);		\
		0;					\
	}						\
})

#define ostack_address(base, size, offset) ({		\
	uint32_t _o = (offset);				\
	if (_o >= (size))				\
		-ENOMEM;				\
	else						\
		(base) + _o;				\
})

#define ostack_depth(base, top) ({			\
	uint32_t _b = (base), _t = (top);		\
	if (_t < _b)					\
		-ENOMEM;				\
	else						\
		_t - _b;				\
})

#endif
#else
#error "Descending Operand Stack not implemented and probably will never be"
#endif

#endif /*JEM*/
#endif /*AVR32_JEM_H_*/
