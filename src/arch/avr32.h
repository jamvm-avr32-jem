/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#define OS_ARCH "avr32"
//#ifndef DEBUG
//#define DEBUG
//#endif

/* Override default min and max heap sizes.  AVR32 machines are
   usually embedded, and the standard defaults are too large. */
#define DEFAULT_MAX_HEAP 8*MB
#define DEFAULT_MIN_HEAP 1*MB

#define HANDLER_TABLE_T static const void
#define DOUBLE_1_BITS 0x3ff0000000000000LL

#define READ_DBL(v,p,l)	v = ((u8)p[0]<<56)|((u8)p[1]<<48)|((u8)p[2]<<40) \
                            |((u8)p[3]<<32)|((u8)p[4]<<24)|((u8)p[5]<<16) \
                            |((u8)p[6]<<8)|(u8)p[7]; p+=8

//extern void setDoublePrecision();
#define FPU_HACK

//see C:\Cygwin\usr\local\avr32-linux\include\bits\atomicity.h
//see http://www.avr32linux.org/twiki/bin/view/Main/AtomicOperations
//see http://www.mail-archive.com/uclibc@uclibc.org/msg00914.html
//http://www.avr32linux.org/twiki/pub/Main/MicroClibcPatches/uClibc-0.9.28-avr32-20060621.patch
//http://www.google.com/codesearch?hl=en&q=+cmpxchg+avr32+show:WwwDfOAAdbA:L2BlPHy6h_g:nY7RakaTOnE&sa=N&cd=1&ct=rc&cs_p=http://gobo.calica.com/packages/official/Linux--2.6.20.4-r1--i686.tar.bz2&cs_f=Linux/2.6.20.4/Resources/Unmanaged/Files/Compile/Sources/linux-2.6.20.4/include/asm-avr32/system.h#first

/*
 * The original avr32 compare_and_swap is different from
 * all other versions (arm, ppc, i386), even varying from
 * implementation in uClibc. It simply returns the value
 * stored in the addr, which makes  no sense for the lock.
 * Replace with the in uClibc implementation
 */
extern int avr32_compare_and_swap(uintptr_t* addr, uintptr_t old_val,
				  uintptr_t new_val);

#define COMPARE_AND_SWAP(addr, old_val, new_val)	\
    avr32_compare_and_swap(addr, old_val, new_val)

#define FLUSH_CACHE(addr, length)

#define LOCKWORD_READ(addr) *addr
#define LOCKWORD_WRITE(addr, value) *addr = value
#define LOCKWORD_COMPARE_AND_SWAP(addr, old_val, new_val) \
        COMPARE_AND_SWAP(addr, old_val, new_val)

#define UNLOCK_MBARRIER() __asm__ __volatile__ ("" ::: "memory")
#define JMM_LOCK_MBARRIER() __asm__ __volatile__ ("" ::: "memory")
#define JMM_UNLOCK_MBARRIER() __asm__ __volatile__ ("" ::: "memory")

//TODO: lto
#define MBARRIER() __asm__ __volatile__ ("" ::: "memory")
//#define MBARRIER() __asm__ __volatile__ ("lock; addl $0,0(%%esp)" ::: "memory")
