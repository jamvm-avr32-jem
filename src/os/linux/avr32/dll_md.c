/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "../../../jam.h"

#ifndef USE_FFI
#include <string.h>
#include "../../../sig.h"

#define RET_VOID    0
#define RET_DOUBLE  1
#define RET_LONG    2
#define RET_FLOAT   3
#define RET_DFLT    4

int nativeExtraArg(MethodBlock *mb) {
	//printf("!!!!!!!!!!nativeExtraArg (name=%s) (type=%s)!!!!!!!\n",mb->name,mb->type);
    int len = strlen(mb->type);
    if(mb->type[len-2] == ')')
        switch(mb->type[len-1]) {
            case 'V':
                return RET_VOID;
            case 'D':
                return RET_DOUBLE;
            case 'J':
                return RET_LONG;
            case 'F':
                return RET_FLOAT;
        }

    return RET_DFLT;
}

//calls the f function pointer and takes care of the return type
#define perform_f(...) do {							\
	switch(ret_type) {							\
	case RET_VOID:								\
		(*(void (*)())f)(__VA_ARGS__);					\
		break;								\
	case RET_DOUBLE:							\
		*(double*)ostack = (*(double (*)())f)(__VA_ARGS__);		\
		ostack += 2;							\
		break;								\
	case RET_LONG:								\
		*(long long*)ostack = (*(long long (*)())f)(__VA_ARGS__);	\
		ostack += 2;							\
		break;								\
	case RET_FLOAT:								\
		*(float*)ostack = (*(float (*)())f)(__VA_ARGS__);		\
		ostack++;							\
		break;								\
	default:								\
		*ostack++ = (*(u4 (*)())f)(__VA_ARGS__);			\
		break;								\
	}									\
} while (0)

#define write_ops_to_stack(x) do {						\
	/*write x operands to the stack	*/					\
	int i;									\
	for(i=0; i < x; i++){							\
		__asm__ __volatile__(						\
		"ld.w r11,%0\n\t"						\
		"st.w --sp,r11"							\
		:: "m"(*--opntr)						\
		:"sp","r11");							\
	}									\
} while (0)

#include <arch/avr32_jem.h>
u4 *callJNIMethod(void *env, Class *class, char *sig, int ret_type, u4 *ostack, unsigned char *f, int args)
{
	u4 *opntr = ostack + args;
	int nrofopsonstack = 0;

	if(class && args > 3)
		nrofopsonstack = args - 3;
	else if(class)
		nrofopsonstack = 0;
	else if(args > 4)
		nrofopsonstack = args - 4;
	else
		nrofopsonstack = 0;

	jam_dprintf("%s: call %p::%p() with %d args, stack %p\n", __func__, class, f, args, ostack);
	switch (args) {
	case 0:
		if(class)
			perform_f(env,class);
		else
			perform_f(env);
		break;
	case 1:
		if(class)
			perform_f(env,class,*--opntr);
		else
			perform_f(env,*--opntr);
		break;
	case 2:
		if(class)
			perform_f(env, class, *(opntr - 2), *(opntr - 1));
		else
			perform_f(env, *(opntr - 2), *(opntr - 1));
		break;
	case 3:
		if(class)
			perform_f(env, class, *(opntr - 3), *(opntr - 2), *(opntr - 1));
		else
			perform_f(env, *(opntr - 3), *(opntr - 2), *(opntr - 1));
		break;
	default:
		write_ops_to_stack(nrofopsonstack);
		if(class)
			perform_f(env, class, *(opntr - 3), *(opntr - 2), *(opntr - 1));
		else
			perform_f(env, *(opntr - 3), *(opntr - 2), *(opntr - 1));
		//Increment stack pointer to loose all operands we put there
		if(nrofopsonstack > 0)
			__asm__ __volatile__("add sp,%0" ::"r"((nrofopsonstack) * sizeof(u4)): "sp");
	}
	
	return ostack;
}

#endif
