/*
 * Copyright (C) 2003, 2004, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

//#include <fpu_control.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <sys/mman.h>
#include <asm/cachectl.h>
#include "jam.h"
#include "arch/avr32_jem.h"

/* Change floating point precision to double (64-bit) from
 * the extended (80-bit) Linux default. */

//void setDoublePrecision() {
//    fpu_control_t cw;
//
//    _FPU_GETCW(cw);
//    cw &= ~_FPU_EXTENDED;
//    cw |= _FPU_DOUBLE;
//    _FPU_SETCW(cw);
//}

#ifdef JEM
extern size_t trap_h_size;

static char *trap_handler(void)
{
	char *trap;

	asm volatile (
		"	mov	%[trap], pc\n"
		"	sub	%[trap], -8\n"	/* Return pointer to trap label */
		"	bral	1f\n"		/* Leave the function */
						/* The actual trap-handler */
		"trap:	mov	r12, pc\n"	/* Store trap entry point */
		"	.globl debug_jem_trap\n"
		"debug_jem_trap:\n"
		"	mov	pc, r10\n"
		"	.globl	trap_h_size\n"	/* and its size */
		"trap_h_size:\n"
		"	.word . - trap\n"
		"	.size trap_h_size, . - trap_h_size\n"
		"1:\n"
		: [trap] "=r" (trap)
		);
	return trap;
}
#endif

//ssrf 5	:set lock bit
//stcond	:store param2 when lock is set
//sreq		:If equal set register to true or false
//brne		:Branch when not equal
//brne 1b	:When the lock wasn't set anymore, try again
//label 1b 2f	:first label named 1/2 before/after
int avr32_compare_and_swap(uintptr_t* addr, uintptr_t old_val,
			   uintptr_t new_val)
{
	int result;
	asm volatile (
		"1:	ssrf	5\n"
			"ld.w	%[ret], %[m]\n"
			"eor	%[ret], %[old]\n"
			"brne	2f\n"
			"stcond	%[m], %[new]\n"
			"brne	1b\n"
		"2:\n"
		: [ret] "=&r" (result), [m] "=m" (*addr)
		: "m" (*addr), [old] "r" (old_val), [new] "r" (new_val)
		: "memory", "cc");
	return result == 0;
}

void initialisePlatform(void)
{
	//TODO: is this needed for avr32?    setDoublePrecision();
#ifdef JEM
	/**
	 * Setup the real traps,the entries are actually loaded when entering 
	 * the interpreter engine
	 */
	char *trap_b, *trap_aligned, *trap_h;
	int i;

	trap_b = mmap(0, 8192, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED | MAP_ANONYMOUS, 0, 0);
	if (trap_b == MAP_FAILED)
		exitVM(1);

	trap_aligned = (void *)(((unsigned long)trap_b + 4095) & ~4095);

	trap_h = trap_handler();

	for (i = 0; i < 24; i++) {
		int j;
		memcpy(trap_aligned + i * 0x80, trap_h, trap_h_size);
#ifdef JEM_DEBUG
		/* Don't even think about jem_printf() here! Although, do think
		 * about it, just that ATM it segfaults */
		printf("%p:", trap_aligned + i * 0x80);
		for (j = 0; j < trap_h_size; j++)
			printf(" %02x", trap_aligned[i * 0x80 + j]);
		printf("\n");
#endif
	}

	/* Clean the data cache, flush the instruction cache */
	syscall(__NR_cacheflush, CACHE_IFLUSH, trap_aligned, 4096);
	syscall(__NR_java_settrap, trap_aligned);
#endif
}
