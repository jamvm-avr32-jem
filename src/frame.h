/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

static inline void *CREATE_TOP_FRAME(ExecEnv *ee, Class *class, MethodBlock *mb,
				     uintptr_t **sp)
{
    Frame *last = ee->last_frame;
    Frame *dummy = (Frame *)(last->ostack + last->mb->max_stack);
    Frame *new_frame;
    uintptr_t *new_ostack;
    void *ret;

    *sp = (uintptr_t*)(dummy + 1);
    new_frame = (Frame *)(*sp + mb->max_locals);
    new_ostack = (uintptr_t *)(new_frame + 1);

    if((char*)(new_ostack + mb->max_stack) > ee->stack_end) {
        if(ee->overflow++) {
            /* Overflow when we're already throwing stack
               overflow.  Stack extension should be enough
               to throw exception, so something's seriously
               gone wrong - abort the VM! */
            printf("Fatal stack overflow!  Aborting VM.\n");
            exitVM(1);
        }
        ee->stack_end += STACK_RED_ZONE_SIZE;
        signalException("java/lang/StackOverflowError", NULL);
        return NULL;
    }

    dummy->mb = NULL;
    dummy->ostack = *sp;
    dummy->prev = last;

    new_frame->mb = mb;
#ifndef JEM
    new_frame->lvars = *sp;
    ret = *sp;
#else
    new_frame->lvars_jem = (uintptr_t *)new_frame;
    ret = new_frame->lvars_jem - 1;
#endif
    new_frame->ostack = new_ostack;

    new_frame->prev = dummy;
    ee->last_frame = new_frame;

    return ret;
}

#define POP_TOP_FRAME(ee)                                       \
    ee->last_frame = ee->last_frame->prev->prev;
