/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include "jam.h"
#include "sig.h"
#include "frame.h"
#include "lock.h"
#include "arch/avr32_jem.h"

#define VA_DOUBLE(args, sp)  *(u8*)sp = va_arg(args, u8); sp+=2

#define VA_SINGLE(args, sp)                   \
    if(*sig == 'L' || *sig == '[')            \
        *sp = va_arg(args, uintptr_t);        \
    else if(*sig == 'F')                      \
        *(u4*)sp = va_arg(args, u4);          \
    else                                      \
        *sp = va_arg(args, u4);               \
    sp++

#ifdef JEM
#define VA_DOUBLE_JEM(args, sp, fp_jem) do {	\
	fp_jem -= 2;				\
	*(u8*)sp = va_arg(args, u8);		\
	*(u8*)fp_jem = *(u8*)sp;		\
} while (0)

#define VA_SINGLE_JEM(args, sp, fp_jem) do {			\
    fp_jem--;							\
    switch (*sig) {						\
    case 'L':							\
	*sp = va_arg(args, uintptr_t);				\
	if (*sp && (!JAM_ON_STACK || IS_JAM_OBJECT(*sp)))	\
	/* If this is an object, we have to calculate a	*/	\
	/* pointer to its data for JEM			*/	\
	    *fp_jem = (uint32_t)INST_DATA((Object*)(*sp));	\
	else							\
	    *fp_jem = *sp;					\
	break;							\
    case '[':							\
	*sp = va_arg(args, uintptr_t);				\
	if (*sp && (!JAM_ON_STACK || IS_JAM_ARRAY(*sp)))	\
	/* Array - similar to object above */			\
	    *fp_jem = (uint32_t)ARRAY_DATA((Object*)(*sp));	\
	else							\
	    *fp_jem = *sp;					\
	break;							\
    case 'F':							\
    default:							\
	*(uint32_t*)sp = va_arg(args, uint32_t);		\
	*(uint32_t*)fp_jem = *(uint32_t*)sp;			\
	break;							\
    }								\
} while (0)
#endif

#define JA_DOUBLE(args, sp)  *(u8*)sp = *args++; sp+=2

#ifdef JEM
#define JA_DOUBLE_JEM(args, sp, fp_jem) do {	\
	fp_jem -= 2;				\
	*(u8*)sp = *args++;			\
	*(u8*)fp_jem = *(u8*)sp;		\
	sp += 2;				\
} while (0)
#endif

#define JA_SINGLE(args, sp)                   \
    switch(*sig) {                            \
        case 'L': case '[': case 'F':         \
            *sp = *(uintptr_t*)args;          \
            break;                            \
        case 'B': case 'Z':                   \
            *sp = *(signed char*)args;        \
            break;                            \
        case 'C':                             \
            *sp = *(unsigned short*)args;     \
            break;                            \
        case 'S':                             \
            *sp = *(signed short*)args;       \
            break;                            \
        case 'I':                             \
            *sp = *(signed int*)args;         \
            break;                            \
    }                                         \
    sp++; args++

#ifdef JEM
#define JA_SINGLE_JEM(args, sp, fp_jem) do {			\
	fp_jem--;						\
	switch (*sig) {						\
        case 'L':						\
	    *sp = *(uintptr_t*)args;				\
	    if (*sp && (!JAM_ON_STACK || IS_JAM_OBJECT(*sp)))	\
		*fp_jem = (uint32_t)INST_DATA((Object*)(*sp));	\
	    else						\
		*fp_jem = *sp;					\
	    break;						\
        case '[':						\
	    *sp = *(uintptr_t*)args;				\
	    if (*sp && (!JAM_ON_STACK || IS_JAM_ARRAY(*sp)))	\
		*fp_jem = (uint32_t)ARRAY_DATA((Object*)(*sp));	\
	    else						\
		*fp_jem = *sp;					\
	    break;						\
        case 'F':						\
            *sp = *(uintptr_t*)args;				\
            *fp_jem = *sp;					\
            break;						\
        case 'B':						\
        case 'Z':						\
            *sp = *(signed char*)args;				\
            *fp_jem = *sp;					\
            break;						\
        case 'C':						\
            *sp = *(unsigned short*)args;			\
            *fp_jem = *sp;					\
            break;						\
        case 'S':						\
            *sp = *(signed short*)args;				\
            *fp_jem = *sp;					\
            break;						\
        case 'I':						\
            *sp = *(signed int*)args;				\
            *fp_jem = *sp;					\
            break;						\
	}							\
	sp++;							\
        args++;							\
} while (0)
#endif

void *executeMethodArgs(Object *ob, Class *class, MethodBlock *mb, ...) {
    va_list jargs;
    void *ret;

    va_start(jargs, mb);
    ret = executeMethodVaList(ob, class, mb, jargs);
    va_end(jargs);

    return ret;
}

void *executeMethodVaList(Object *ob, Class *class, MethodBlock *mb, va_list jargs)
{
    char *sig = mb->type;
    ExecEnv *ee = getExecEnv();
    uintptr_t *sp;
    uintptr_t tmp[2];
#ifdef JEM
    uint32_t *fp_jem = NULL;
#endif
    void *ret;

    ret = CREATE_TOP_FRAME(ee, class, mb, &sp);
    if (!ret)
	    return ret;

    /* copy args onto stack */
    /* Do not touch the original stack frame,just allocate lvars_jem from the
     * native heap and free it after finishing method execution */
#ifdef JEM
    jam_dprintf("[%s] execute method (locals %d, args %d) %s type %s signature %s on obj %x\n",
                __func__, mb->max_locals, mb->args_count, mb->name, sig, mb->signature, ob);
    if(mb->max_locals > 0)
        fp_jem = ee->last_frame->lvars_jem;
#endif

    if (ob) {
#ifndef JEM
        *sp++ = (uintptr_t) ob; /* push receiver first */
#else
	if(!JAM_ON_STACK || IS_JAM_OBJECT(ob))
	    *(--fp_jem) = (uint32_t)INST_DATA(ob);
	else
	    *(--fp_jem) = (uint32_t)ob;
#endif
    }

#ifndef JEM
    SCAN_SIG(sig, VA_DOUBLE(jargs, sp), VA_SINGLE(jargs, sp));
#else
    sp = tmp;
    SCAN_SIG(sig, VA_DOUBLE_JEM(jargs, sp, fp_jem), VA_SINGLE_JEM(jargs, sp, fp_jem));

    if (JAM_ON_STACK && ob && !IS_JAM_OBJECT(ob))
	ob = JAM_OBJECT((uintptr_t *)ob);
#endif

    if(mb->access_flags & ACC_SYNCHRONIZED)
        objectLock(ob ? ob : (Object*)mb->class);

    if(mb->access_flags & ACC_NATIVE) {
        jam_dprintf("[%s] execute native method\n", __func__);
        (*(uintptr_t *(*)(Class*, MethodBlock*, uintptr_t*))mb->native_invoker)(class, mb, ret);
    } else
        executeJava();

    if(mb->access_flags & ACC_SYNCHRONIZED)
        objectUnlock(ob ? ob : (Object*)mb->class);

    POP_TOP_FRAME(ee);
#ifdef JEM

    /*
     * Since ret might be returnAddressType, here convert back to JAM
     * objrefs/arrayrefs from JEM refs
     */
    if (*(uint32_t *)ret)
	    switch (*(strchr(mb->type, ')') + 1)) {
	    case 'L':
		    *(uint32_t*)ret = (uint32_t)JAM_OBJECT((uintptr_t *)*(uint32_t *)ret);
		    break;
	    case '[':
		    *(uint32_t*)ret = (uint32_t)JAM_ARRAY((uintptr_t *)*(uint32_t *)ret);
		    break;
	    }
    jam_dprintf("[%s] %s: return 0x%x @ %p\n", __func__, mb->type, *(uint32_t *)ret, ret);
#endif
    return ret;
}

void *executeMethodList(Object *ob, Class *class, MethodBlock *mb, u8 *jargs) {
    char *sig = mb->type;

    ExecEnv *ee = getExecEnv();
    uintptr_t *sp;
    void *ret;
#ifdef JEM
    uintptr_t *fp_jem;
#endif

    ret = CREATE_TOP_FRAME(ee, class, mb, &sp);
    if (!ret)
	    return ret;

    /* copy args onto stack */
    /* Do not touch the original stack frame,just allocate lvars_jem from the
     * native heap and free it after finishing method execution */
#ifdef JEM
    jam_dprintf("[executeMethodList] execute method (args %d) %s\n", mb->max_locals, mb->name);
    fp_jem = ee->last_frame->lvars_jem;
#endif

    if(ob){
#ifdef JEM
	if(!JAM_ON_STACK || IS_JAM_OBJECT(ob))
	    *(--fp_jem) = (uint32_t)INST_DATA(ob);
	else
	    *(--fp_jem) = (uint32_t)ob;
#endif
        *sp++ = (uintptr_t) ob; /* push receiver first */
    }
#ifdef JEM
    SCAN_SIG(sig, JA_DOUBLE_JEM(jargs, sp, fp_jem), JA_SINGLE_JEM(jargs, sp, fp_jem));
    if (JAM_ON_STACK && ob && !IS_JAM_OBJECT(ob))
	ob = JAM_OBJECT((uintptr_t *)ob);
#else
    SCAN_SIG(sig, JA_DOUBLE(jargs, sp), JA_SINGLE(jargs, sp));
#endif
    if(mb->access_flags & ACC_SYNCHRONIZED)
        objectLock(ob ? ob : (Object*)mb->class);

    if(mb->access_flags & ACC_NATIVE)
        (*(uintptr_t *(*)(Class*, MethodBlock*, uintptr_t*))mb->native_invoker)(class, mb, ret);
    else
        executeJava();

    if(mb->access_flags & ACC_SYNCHRONIZED)
        objectUnlock(ob ? ob : (Object*)mb->class);

    POP_TOP_FRAME(ee);

#ifdef JEM
    switch(*(strchr(mb->type,')')+1)){
	case 'L':
	    *(uint32_t *)ret = (uint32_t)JAM_OBJECT((uintptr_t *)*(uint32_t *)ret);
	    break;
	case '[':
	    *(uint32_t *)ret = (uint32_t)JAM_ARRAY((uintptr_t *)*(uint32_t *)ret);
	    break;
    }
    jam_dprintf("[%s] return 0x%x\n", __func__, *(uint32_t *)ret);
#endif

    return ret;
}
