/*
 * Copyright (C) 2003, 2004, 2005, 2006, 2007
 * Robert Lougher <rob@lougher.org.uk>.
 *
 * This file is part of JamVM.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "jam.h"
#include "arch/avr32_jem.h"

#ifndef NO_JNI
#include "hash.h"
#include "jni.h"
#include "natives.h"

/* Set by call to initialise -- if true, prints out
    results of dynamic method resolution */
static int verbose;

extern int nativeExtraArg(MethodBlock *mb);
extern uintptr_t *callJNIMethod(void *env, Class *class, char *sig, int extra,
                                uintptr_t *ostack, unsigned char *native_func, int args);
extern struct _JNINativeInterface Jam_JNINativeInterface;
extern int initJNILrefs();
extern JavaVM invokeIntf; 

#define HASHTABSZE 1<<4
static HashTable hash_table;
void *lookupLoadedDlls(MethodBlock *mb);
#endif

/* Trace library loading and method lookup */
#ifdef TRACEDLL
#define TRACE(fmt, ...) jam_printf(fmt, ## __VA_ARGS__)
#else
#define TRACE(fmt, ...)
#endif

char *mangleString(char *utf8) {
    int len = utf8Len(utf8);
    unsigned short *unicode = (unsigned short*) sysMalloc(len * 2);
    char *mangled, *mngldPtr;
    int i, mangledLen = 0;

    convertUtf8(utf8, unicode);

    /* Work out the length of the mangled string */

    for(i = 0; i < len; i++) {
        unsigned short c = unicode[i];
        switch(c) {
            case '_':
            case ';':
            case '[':
                mangledLen += 2;
                break;

           default:
                mangledLen += isalnum(c) ? 1 : 6;
                break;
        }
    }

    mangled = mngldPtr = (char*) sysMalloc(mangledLen + 1);

    /* Construct the mangled string */

    for(i = 0; i < len; i++) {
        unsigned short c = unicode[i];
        switch(c) {
            case '_':
                *mngldPtr++ = '_';
                *mngldPtr++ = '1';
                break;
            case ';':
                *mngldPtr++ = '_';
                *mngldPtr++ = '2';
                break;
            case '[':
                *mngldPtr++ = '_';
                *mngldPtr++ = '3';
                break;

            case '/':
                *mngldPtr++ = '_';
                break;

            default:
                if(isalnum(c))
                    *mngldPtr++ = c;
                else
                    mngldPtr += sprintf(mngldPtr, "_0%04x", c);
                break;
        }
    }

    *mngldPtr = '\0';

    sysFree(unicode);
    return mangled;
}

char *mangleClassAndMethodName(MethodBlock *mb) {
    char *classname = CLASS_CB(mb->class)->name;
    char *methodname = mb->name;
    char *nonMangled = (char*) sysMalloc(strlen(classname) + strlen(methodname) + 7);
    char *mangled;

    sprintf(nonMangled, "Java/%s/%s", classname, methodname);

    mangled = mangleString(nonMangled);
    sysFree(nonMangled);
    return mangled;
}

char *mangleSignature(MethodBlock *mb) {
    char *type = mb->type;
    char *nonMangled;
    char *mangled;
    int i;

    /* find ending ) */
    for(i = strlen(type) - 1; type[i] != ')'; i--);

    nonMangled = (char *) sysMalloc(i);
    strncpy(nonMangled, type + 1, i - 1);
    nonMangled[i - 1] = '\0';
    
    mangled = mangleString(nonMangled);
    sysFree(nonMangled);
    return mangled;
}

void *lookupInternal(MethodBlock *mb) {
    ClassBlock *cb = CLASS_CB(mb->class);
    int i;

    TRACE("<DLL: Looking up %s internally>\n", mb->name);

    /* First try to locate the class */
    for(i = 0; native_methods[i].classname &&
        (strcmp(cb->name, native_methods[i].classname) != 0); i++);

    if(native_methods[i].classname) {
        VMMethod *methods = native_methods[i].methods;

        /* Found the class -- now try to locate the method */
        for(i = 0; methods[i].methodname &&
            (strcmp(mb->name, methods[i].methodname) != 0); i++);

        if(methods[i].methodname) {
            if(verbose)
                jam_printf("internal");

#ifdef JEM
            /*
             * Since native method would be invoked in separate stack storage in
             * JEM, still use resolveNativeWrapper as our native_invoker here,
             * which would pay off the performance lost
             */
            return (void*)methods[i].method;
#else
            /* Found it -- set the invoker to the native method */
            return mb->native_invoker = (void*)methods[i].method;
#endif
        }
    }

    return NULL;
}

void *resolveNativeMethod(MethodBlock *mb) {
    void *func;

    if(verbose) {
        char *classname = slash2dots(CLASS_CB(mb->class)->name);
        jam_printf("[Dynamic-linking native method %s.%s ... ", classname, mb->name);
        sysFree(classname);
    }

    /* First see if it's an internal native method */
    func  = lookupInternal(mb);

#ifndef NO_JNI
    if(func == NULL)
        func = lookupLoadedDlls(mb);
#endif

    if(verbose)
        jam_printf("]\n");

    return func;
}

uintptr_t *resolveNativeWrapper(Class *class, MethodBlock *mb, uintptr_t *ostack) {
    void *func = resolveNativeMethod(mb);

    if(func == NULL) {
        signalException("java/lang/UnsatisfiedLinkError", mb->name);
        return ostack;
    }
#ifdef JEM
	/*
	 * In order to avoid the overhead to modify each native method
	 * implementation, we modify the JamVM's native invoker wrapper.
	 * Since all objrefs/arrayrefs residing in ostack are all JEM aware,
	 * JamVM's native methods only accept JamVM's "Object*". So here, we
	 * prepare a separate temp stack storage, layout as below:
	 *                  | RET2 |
	 *                  | RET1 |  --- used to store return value(32/64bit)
	 *                  | ARGN |
	 *                  | ...  |
	 *                  | ARG1 |  --- method arguments
	 * On return, the return value should be copied back to original ostack
	 * and objrefs/arrayrefs should be restored into JEM type
	 */
    uint32_t *ret_stack, *temp;
    int i;
    uint32_t *temp_stack = sysMalloc((mb->args_count + 2) * sizeof(uint32_t));

    memcpy(temp_stack, ostack, mb->args_count * sizeof(uint32_t));

//check if it is an valid object ref (see alloc.c)
#define OBJECT_GRAIN  8
#define IS_OBJECT(ptr) !(((uintptr_t)(ptr))&(OBJECT_GRAIN-1))

    TRACE("DLL: invoke native method (%s:%s) %d on JEM\n", mb->name, mb->type, mb->args_count);

    extern void *getHeapBase(void);
    //scan arguments
    for(i = 0; i < mb->args_count; i++){
	if(temp_stack[i] != 0x0 && temp_stack[i] > (uint32_t)getHeapBase()){
	    Object *obj = JAM_OBJECT((uintptr_t *)temp_stack[i]);
	    if(!IS_OBJECT(obj))
		obj = JAM_ARRAY((uintptr_t *)temp_stack[i]);

	    if(!JAM_ON_STACK || IS_JAM_OBJECT(obj))
		temp_stack[i] = (uint32_t)obj;
	}
    }

    temp = temp_stack;

    ret_stack = (*(uintptr_t *(*)(Class*, MethodBlock*, uintptr_t*))func)(class, mb, temp_stack);

    TRACE("DLL: native method return %x\n", ret_stack);
    ostack += mb->args_count;
    /* copy result to ostack */
    switch(*(strchr(mb->type,')')+1)){
        case 'J':
        case 'D':
            //64 bits
	    ostack_push_u64(NULL, 0, ostack, ret_stack - 2);
            break;
        case 'L':
            //objref
	    TRACE("DLL: return objref\n");
	    ostack_push_u32(NULL, 0, ostack, *(ret_stack - 1) ? (uint32_t)INST_DATA((Object*)*(ret_stack - 1)) : 0x0);
            break;
        case '[':
            //array ref
	    TRACE("DLL :return arrayref\n");
	    ostack_push_u32(NULL, 0, ostack, *(ret_stack - 1) ? (uint32_t)ARRAY_DATA((Object*)*(ret_stack - 1)) : 0x0);
            break;
	case 'V':
	    //void
	    break;
	default:
	    //32 bits
	    ostack_push_u32(NULL, 0, ostack, *(ret_stack - 1));
	    break;
    }

    TRACE("DLL: native invoker quit\n");
    sysFree(temp);
    return ostack;

#else
    return (*(uintptr_t *(*)(Class*, MethodBlock*, uintptr_t*))func)(class, mb, ostack);
#endif
}

void initialiseDll(InitArgs *args) {
#ifndef NO_JNI
    /* Init hash table, and create lock */
    initHashTable(hash_table, HASHTABSZE, TRUE);
#endif
    verbose = args->verbosedll;
}

#ifndef NO_JNI
typedef struct {
    char *name;
    void *handle;
    Object *loader;
} DllEntry;

int dllNameHash(char *name) {
    int hash = 0;

    while(*name)
        hash = hash * 37 + *name++;

    return hash;
}

int resolveDll(char *name, Object *loader) {
    DllEntry *dll;

    TRACE("<DLL: Attempting to resolve library %s>\n", name);

#define HASH(ptr) dllNameHash(ptr)
#define COMPARE(ptr1, ptr2, hash1, hash2) \
                  ((hash1 == hash2) && (strcmp(ptr1, ptr2->name) == 0))
#define PREPARE(ptr) ptr
#define SCAVENGE(ptr) FALSE
#define FOUND(ptr) ptr

    /* Do not add if absent, no scavenge, locked */
    findHashEntry(hash_table, name, dll, FALSE, FALSE, TRUE);

    if(dll == NULL) {
        DllEntry *dll2;
        void *onload, *handle = nativeLibOpen(name);

        if(handle == NULL)
            return FALSE;

        TRACE("<DLL: Successfully opened library %s>\n", name);

        if((onload = nativeLibSym(handle, "JNI_OnLoad")) != NULL) {
            int ver = (*(jint (*)(JavaVM*, void*))onload)(&invokeIntf, NULL);

            if(ver != JNI_VERSION_1_2 && ver != JNI_VERSION_1_4) {
                TRACE("<DLL: JNI_OnLoad returned unsupported version %d.\n>", ver);
                return FALSE;
            }
        }

        dll = (DllEntry*)sysMalloc(sizeof(DllEntry));
        dll->name = strcpy((char*)sysMalloc(strlen(name)+1), name);
        dll->handle = handle;
        dll->loader = loader;

#undef HASH
#undef COMPARE
#define HASH(ptr) dllNameHash(ptr->name)
#define COMPARE(ptr1, ptr2, hash1, hash2) \
                  ((hash1 == hash2) && (strcmp(ptr1->name, ptr2->name) == 0))

        /* Add if absent, no scavenge, locked */
        findHashEntry(hash_table, dll, dll2, TRUE, FALSE, TRUE);
    } else
        if(dll->loader != loader)
            return FALSE;

    return TRUE;
}

char *getDllPath() {
    char *env = nativeLibPath();
    return env ? env : "";
}

char *getBootDllPath() {
    return CLASSPATH_INSTALL_DIR"/lib/classpath";
}

char *getDllName(char *name) {
   return nativeLibMapName(name);
}

void *lookupLoadedDlls0(char *name, Object *loader) {
    TRACE("<DLL: Looking up %s loader %x in loaded DLL's>\n", name, loader);

#define ITERATE(ptr)                                          \
{                                                             \
    DllEntry *dll = (DllEntry*)ptr;                           \
    if(dll->loader == loader) {                               \
        void *sym = nativeLibSym(dll->handle, name);          \
        if(sym != NULL)                                       \
            return sym;                                       \
    }                                                         \
}

    hashIterate(hash_table);
    return NULL;
}

void unloadDll(DllEntry *dll) {
    void *on_unload;

    TRACE("<DLL: Unloading DLL %s\n", dll->name);

    if((on_unload = nativeLibSym(dll->handle, "JNI_OnUnload")) != NULL)
        (*(void (*)(JavaVM*, void*))on_unload)(&invokeIntf, NULL);

    nativeLibClose(dll->handle);
    sysFree(dll);
}

#undef ITERATE
#define ITERATE(ptr)                                          \
{                                                             \
    DllEntry *dll = (DllEntry*)ptr;                           \
    if(isMarked(dll->loader))                                 \
        threadReference(&dll->loader);                        \
}

void threadLiveClassLoaderDlls() {
    hashIterate(hash_table);
}

void unloadClassLoaderDlls(Object *loader) {
    int unloaded = 0;

    TRACE("<DLL: Unloading DLLs for loader %x\n", loader);

#undef ITERATE
#define ITERATE(ptr)                                          \
{                                                             \
    DllEntry *dll = (DllEntry*)*ptr;                          \
    if(dll->loader == loader) {                               \
        unloadDll(dll);                                       \
        *ptr = NULL;                                          \
        unloaded++;                                           \
    }                                                         \
}

    hashIterateP(hash_table);

    if(unloaded) {
        int size;

        /* Update count to remaining number of DLLs */
        hash_table.hash_count -= unloaded;

        /* Calculate nearest multiple of 2 larger than count */
        for(size = 1; size < hash_table.hash_count; size <<= 1);

        /* Ensure new table is less than 2/3 full */
        size = hash_table.hash_count*3 > size*2 ? size<< 1 : size;

        resizeHash(&hash_table, size);
    }
}

static void *env = &Jam_JNINativeInterface;

uintptr_t *callJNIWrapper(Class *class, MethodBlock *mb, uintptr_t *ostack) {
    TRACE("<DLL: Calling JNI method %s.%s%s>\n", CLASS_CB(class)->name, mb->name, mb->type);

    if(!initJNILrefs())
        return NULL;

    return callJNIMethod(&env, (mb->access_flags & ACC_STATIC) ? class : NULL,
                         mb->type, mb->native_extra_arg, ostack, mb->code, mb->args_count);
}

void *lookupLoadedDlls(MethodBlock *mb) {
    Object *loader = (CLASS_CB(mb->class))->class_loader;
    char *mangled = mangleClassAndMethodName(mb);
    void *func;

    func = lookupLoadedDlls0(mangled, loader);

    if(func == NULL) {
        char *mangledSig = mangleSignature(mb);
        char *fullyMangled = (char*)sysMalloc(strlen(mangled)+strlen(mangledSig)+3);

        sprintf(fullyMangled, "%s__%s", mangled, mangledSig);
        func = lookupLoadedDlls0(fullyMangled, loader);

        sysFree(fullyMangled);
        sysFree(mangledSig);
    }

    sysFree(mangled);

    if(func) {
        if(verbose)
            jam_printf("JNI");

        mb->code = (unsigned char *) func;
        mb->native_extra_arg = nativeExtraArg(mb);
#ifdef JEM
        return (void*)callJNIWrapper;
#else
        return mb->native_invoker = (void*) callJNIWrapper;
#endif
    }

    return NULL;
}
#endif

